/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Divider from '@material-ui/core/Divider';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
		})
	}

	componentDidMount(){

	}

  render(){
		// #2B678C

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'10px'}}>
	              <Container>
									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
										<Row style={{width:'100%'}}>
		                  <Col className="ml-auto mr-auto">
												<h4 className="centerobject" style={{color: '#000000'}}>
													Activity
												</h4>
											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

					<div style={{marginBottom:'10px'}}>
					</div>
	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
