/*eslint-disable*/
import React from "react";

// reactstrap components
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";
// core comments


function PresentationHeader() {
  return (
    <>
      <div className="wrapper">
        <div
          className="page-header section-dark"
          style={{
            backgroundImage:
              "url(" + require("assets/img/sections/pk-pro-cover.jpg") + ")"
          }}
        >
          <div className="content-center">
            <Container>
              <div className="title-brand">
                <h1 className="presentation-title" style={{fontSize: '70px'}}>
                  100 Ideas
                </h1>
                <h1 className="presentation-title" style={{fontSize: '30px'}}>
                  have been submitted
                </h1>
                <div className="type">1.0</div>
                <div className="fog-low">
                  <img
                    alt="..."
                    src={require("assets/img/sections/fog-low.png")}
                  />
                </div>
                <div className="fog-low right">
                  <img
                    alt="..."
                    src={require("assets/img/sections/fog-low.png")}
                  />
                </div>
              </div>
              <h2 className="presentation-subtitle text-center" style={{marginTop:'100px'}}>
                Revolutionary or Simple - We want to hear your Idea!
              </h2>
            </Container>

          </div>
        </div>
      </div>
    </>
  );
}

export default PresentationHeader;
