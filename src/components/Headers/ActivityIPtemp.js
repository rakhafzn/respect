/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Divider from '@material-ui/core/Divider';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
			list_activity : [],
			list_activity2 : [],
			filename_download : null,
			page : 'aktivitas',
			superior : false,
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			email : cookies.get('user_email'),
		})
	}

	componentDidMount(){
		var email;
		axios.get('https://training-api.pti-cosmetics.com/idea_superior_list?email=eq.'+this.state.email)
		.then(response => {
			console.log(response.data)

			axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?order=timestamp.desc&email_superior=eq.'+this.state.email)
			.then(response => {
				console.log(response.data)
				this.setState({
					list_activity: response.data,
					superior: true,
				})
			})
			.catch(error => {
				console.log(error)
			})
			axios.get('https://training-api.pti-cosmetics.com/idea_task_log?order=timestamp.desc&email_superior=eq.'+this.state.email)
			.then(response => {
				console.log(response.data)
				this.setState({
					list_activity2: response.data,
					superior: true,
				})
			})
			.catch(error => {
				console.log(error)
			})
		})
		.catch(error => {
			console.log(error)
			axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?order=timestamp.desc&email=eq.'+this.state.email)
			.then(response => {
				console.log(response.data)
				this.setState({
					list_activity: response.data
				})
			})
			.catch(error => {
				console.log(error)
			})
			axios.get('https://training-api.pti-cosmetics.com/idea_task_log?order=timestamp.desc&email=eq.'+this.state.email)
			.then(response => {
				console.log(response.data)
				this.setState({
					list_activity2: response.data
				})
			})
			.catch(error => {
				console.log(error)
			})
		})


		// axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?order=timestamp.desc&email=eq.'+this.state.email)
		// .then(response => {
		// 	// console.log(response.data)
		// 	this.setState({
		// 		list_activity: response.data
		// 	})
		// })
		// .catch(error => {
		// 	console.log(error)
		// })
	}

	handleClickPageFile = event => {
		this.setState({
			page : 'file'
		})
	}

	handleClickPageAktivitas = event => {
		this.setState({
			page : 'aktivitas'
		})
	}

	handleClickChooseFileDownload = (event,obj) => {
		// console.log(event)
		// console.log(obj)
		this.setState({
			filename_download : event
		})
	}

  render(){
		// console.log(this.state.email)
		console.log(this.state.list_activity)
		console.log(this.state.list_activity2)
		console.log(this.state.page)

		var openfile = 'https://testing-bivi-007.pti-cosmetics.com/bivi-api/assets/'+this.state.filename_download
		console.log(openfile)


		const page = this.state.page
		const handleClickPageFile = this.handleClickPageFile.bind(this)
		const handleClickPageAktivitas = this.handleClickPageAktivitas.bind(this)
		const handleClickChooseFileDownload = this.handleClickChooseFileDownload
		const email = this.state.userloggedin.display_name
		const list_activity = this.state.list_activity
		const list_activity2 = this.state.list_activity2
		console.log(email)

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			return (
				<>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'100px'}}>
	              <Container>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
										<Row style={{width:'100%'}}>
		                  <Col className="ml-auto mr-auto">
												<Card className="card-register" style ={{
													backgroundColor:'#ffffff',
													margin:'0px',
													minHeight:'0px',
													width:'100%',
													boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
												}}>

													{(function() {
														if(page == 'aktivitas') {
															return(
																<Row>
																	<Col xs="6" sm="4">
																		<button
																			type="button"
																			class="btn btn-warning"
																			style={{margin:'0px', padding:'0px', width:'100%'}}
																		>
																			Aktivitas
																		</button>
																	</Col>
																	<Col xs="6" sm="4">
																		<button
																			class="btn btn-primary btn-neutral "
																			onClick={handleClickPageFile}
																			style={{margin:'0px', padding:'0px'}}
																		>
																			File
																		</button>
																	</Col>

																	<div style={{marginBottom:'10px', paddingTop:'10px', width:'100%'}}>
																		<Row>
																			{list_activity2.map((row)=> (
																				<div>
																					<Divider style={{marginLeft : '15px', marginRight:'15px', marginTop:'5px', marginBottom:'5px'}}/>

																					<Col xs="6" sm="4" style={{color:'black'}}>
																						<p style={{color:'#000000', textAlign:'left'}}>
																							{row.timestamp.slice(0,10)}
																						</p>
																					</Col>
																					<div style={{paddingLeft : '15px', paddingRight:'15px'}}>
																						<Divider style={{marginTop:'5px', marginBottom:'5px'}}/>
																						{(function(){
																							if (row.user === email) {
																								if (row.task === 'submit') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												Kamu menambahkan project baru <b>{row.idea_title}</b>
																										</p>
																									)
																								} else if (row.task === 'update') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												Kamu memperbaharui project <b>{row.idea_title}</b>
																										</p>
																									)
																								} else if (row.task === 'Approve') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												Kamu menyetujui project <b>{row.idea_title}</b>
																										</p>
																									)
																								} else if (row.task === 'Revision') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												Kamu meminta project <b>{row.idea_title}</b> untuk diperbaharui
																										</p>
																									)
																								} else if (row.task === 'Archived') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												Kamu menyimpan project <b>{row.idea_title}</b> untuk Improvement Project periode berikutnya
																										</p>
																									)
																								}

																							} else {
																								if (row.task === 'submit') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												<b>{row.user}</b> menambahkan project baru <b>{row.idea_title}</b>
																										</p>
																									)
																								} else if (row.task === 'update') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												<b>{row.user}</b> memperbaharui project <b>{row.idea_title}</b>
																										</p>
																									)
																								} else if (row.task === 'Approve') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												<b>{row.user}</b> menyetujui project <b>{row.idea_title}</b>
																										</p>
																									)
																								} else if (row.task === 'Revision') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												<b>{row.user}</b> meminta project <b>{row.idea_title}</b> untuk diperbaharui
																										</p>
																									)
																								} else if (row.task === 'Archived') {
																									return(
																										<p style={{color:'#000000', textAlign:'left'}}>
																												<b>{row.user}</b> menyimpan project <b>{row.idea_title}</b> untuk Improvement Project periode berikutnya
																										</p>
																									)
																								}
																							}
																						})()}

																					</div>
																				</div>
																			))}
																		</Row>
																	</div>

																</Row>
															)
														} else {
															return(
																<Row>
																	<Col xs="6" sm="4">
																		<button
																			class="btn btn-primary btn-neutral "
																			onClick={handleClickPageAktivitas}
																			style={{margin:'0px', padding:'0px'}}
																		>
																			Aktivitas
																		</button>
																	</Col>
																	<Col xs="6" sm="4">
																		<button
																			type="button"
																			class="btn btn-warning"
																			style={{margin:'0px', padding:'0px', width:'100%'}}
																		>
																			File
																		</button>
																	</Col>

																	<div style={{marginBottom:'10px', paddingTop:'10px', width:'100%'}}>
																		{list_activity.map((row)=> (
																			<div>
																				<Row>
																					<Col xs="1" sm="1" style={{padding:'0px'}}>
																						<img style={{width:'25px', paddingTop:'20px', paddingLeft:'5px'}}
																							alt="..."
																							src={require("assets/img/ideabox/file2.svg")}
																						/>
																					</Col>
																					<Col xs="7" sm="7" style={{padding:'0px'}}>
																						<div style={{paddingLeft : '0px', paddingRight:'0px'}}>
																							<Divider style={{marginTop:'5px', marginBottom:'5px'}}/>
																							<a
																								href={openfile}
																								target="_blank"
																							>
																								<h4 className="centerobject"
																									onClick={handleClickChooseFileDownload.bind(this,row.filename)}
																								 	style={{
																										paddingLeft:'5px',
																										textAlign: 'left',
																										marginTop: '10px',
																										marginBottom: '5px',
																										fontFamily: 'Montserrat',
																										fontWeight:'400',
																										fontSize: '15px',
																										color: '#2B678C'
																									}}
																								>
																									{row.filename}
																								</h4>
																							</a>
																							<h4 className="centerobject" style={{
																								paddingLeft:'5px',
																								textAlign: 'left',
																								marginTop: '5px',
																								marginBottom: '5px',
																								fontFamily: 'Montserrat',
																								fontWeight:'400',
																								fontSize: '15px',
																								color: '#bbbbbb'
																							}}>
																								{row.email.slice(0,row.email.length-18)}
																							</h4>
																						</div>
																					</Col>
																					<Col xs="4" sm="4" style={{padding:'0px'}}>
																						<div style={{paddingLeft : '0px', paddingRight:'0px'}}>
																							<Divider style={{marginTop:'5px', marginBottom:'5px'}}/>
																							<h4 className="centerobject" style={{
																								padding:'0px',
																								textAlign: 'center',
																								marginTop: '10px',
																								marginBottom: '0px',
																								fontFamily: 'Montserrat',
																								fontWeight:'400',
																								fontSize: '12px',
																								color: '#2B678C'
																							}}>
																								{row.timestamp.slice(0,10)}
																							</h4>
																							<h4 className="centerobject" style={{
																								padding:'0px',
																								textAlign: 'center',
																								marginTop: '10px',
																								marginBottom: '0px',
																								fontFamily: 'Montserrat',
																								fontWeight:'400',
																								fontSize: '12px',
																								color: '#2B678C'
																							}}>
																								{row.timestamp.slice(11,16)}
																							</h4>
																						</div>
																					</Col>
																				</Row>
																			</div>
																		))}
																	</div>
																</Row>
															)
														}
													})()}
												</Card>
											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

					<div style={{marginBottom:'100px'}}>
					</div>
	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
