/*eslint-disable*/
import React from "react";

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			color: [],
      idea_counter: null,
		};
	}

  componentDidMount() {
		const cookies = new Cookies()

    axios.get('https://training-api.pti-cosmetics.com/v_idea_submited?select=id')
		.then(response => {
      console.log(response.data.length)
      this.setState({
				userloggedin: cookies.get('userloggedin'),
				login_status: cookies.get('login_status'),
        idea_counter : response.data.length,
  		})
    })
		.catch(error => {
			console.log(error)
		})
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
    console.log(this.state.login_status)
		if (this.state.login_status === true || this.state.login_status === 'true') {
			var login_name = this.state.userloggedin.display_name
		} else {
			var login_name = null
		}
		
    return (
      <>
        <div >
          <div
					style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'20px'}}
            className="section"
          >
            <div style={{marginTop:'65px'}}>
              <Container>
								<div className="title-brand">
                  <h1 style={{fontSize: '60px', fontFamily: 'Montserrat', fontWeight:'600', marginTop:'0px'}}>
                    <b>{this.state.idea_counter} Ideas</b>
                  </h1>
                  <h1 style={{fontSize: '30px', fontFamily: 'Montserrat', marginTop:'0px'}}>
                    <b>have been submitted</b>
                  </h1>

									<div className="centerobject" style={{width:'100%'}}>
                    <img
                      className="centerobject fog-low"
                      alt="..."
                      src={require("assets/img/sections/fog-low.png")}
                      style={{marginLeft:'0%', width:'100%', marginTop:'-50%'}}
                    />
                  </div>
                </div>
                <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'12px'}}>
                	Thank you for 1.184 submitted ideas per 12 Jun 2020.
                </h2>
                <h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'12px'}}>
                	Innovation Mayday has temporarily closed.
                </h2>
                <h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'12px'}}>
                	Keep submit your ideas, and they will be reviewed in next period :)
                </h2>
              </Container>

            </div>
          </div>
        </div>
      </>
    );
  }

}

export default PresentationHeaderIdeation;
