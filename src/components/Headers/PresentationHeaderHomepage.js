/*eslint-disable*/
import React from "react";

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Card, CardBody, Button, CardTitle, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			color: [],
			nextPage : null,
		};
	}

  componentDidMount() {
		const cookies = new Cookies()
    this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
  	})
	}

	handleClickGoToIM = () =>{
		// console.log(event.target.value)
		this.setState({
			nextPage : 'IM'
		})
	}

	handleClickGoToIP = () =>{
		// console.log(event.target.value)
		this.setState({
			nextPage : 'IP'
		})
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
		console.log(this.state.login_status)
		console.log(this.state.nextPage)

		const userloggedin = this.state.userloggedin
		const handleClickGoToIM = this.handleClickGoToIM.bind(this)
		const handleClickGoToIP = this.handleClickGoToIP.bind(this)

		if (this.state.nextPage !== null && this.state.nextPage === 'IM') {
			return(
				<Redirect from="*" to="/im" />
			)
		} else if (this.state.nextPage !== null && this.state.nextPage === 'IP') {
			return(
				<Redirect from="*" to="/ip" />
			)
		} else {

	    return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'110px'}}>
	              <Container>
									{(function(){
										if(userloggedin === null || userloggedin === undefined){
											return(
												<h1 style={{
													fontSize: '15px',
													fontFamily: 'Montserrat',
													fontWeight:'300',
													marginTop:'40px',
													color:'#000000'
												}}>
			                    Selamat datang paragonian
			                  </h1>
											)
										} else {
											return(
												<h1 style={{
													fontSize: '15px',
													fontFamily: 'Montserrat',
													fontWeight:'300',
													marginTop:'40px',
													color:'#000000'
												}}>
			                    Hallo, {userloggedin.display_name}!
			                  </h1>
											)
										}
									})()}

									<h1 style={{
										fontSize: '20px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										marginTop:'10px',
										color:'#2B678C'
									}}>
										Apa <b style={{backgroundColor:'#2B678C', color:'#ffffff'}}><i>"aha moment" </i></b> mu hari ini ?
									</h1>

									<div className="title-brand">

										<Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px', display: 'flex',marginTop:'20px'}}>
											<Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>
												<Grid item xs={12}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
													<Card
														data-background="color"
														data-color="white"
														style={{margin:'0px', backgroundColor:'#2B678C', paddingTop:'10px'}}
													>
														<CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
															<Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px', display: 'flex',marginTop:'	0px'}}>
																<Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>
																	<Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
																		<img style={{width:'100px'}}
																			alt="..."
																			onClick={this.handleClickGoToIM.bind(this)}
																			src={require("assets/img/ideabox/im_round.png")}
																		/>
																	</Grid>
																	<Grid item xs={8}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
																		<h4 style={{
																			padding:'0px',
																			margin: '0px',
																			textAlign: 'left',
																			fontFamily: 'Montserrat',
																			fontWeight:'600',
																			fontSize: '15px',
																			color: '#ffffff',
																		}}>
																			Innovation Mayday
																		</h4>
																		<br/>
																		<h4 style={{
																			padding:'0px',
																			margin: '0px',
																			textAlign: 'left',
																			fontFamily: 'Montserrat',
																			fontWeight:'300',
																			fontSize: '12px',
																			color: '#ffffff',
																		}}>
																			Kamu dapat submit ide project untuk departemenmu sendiri atau departemen lainnya sebanyak mungkin
																		</h4>
																		<Button
																			className="btn-round"
																			color="default"
																			type="button"
																			style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'100%', marginTop:'10px', marginBottom:'10px'}}
																			onClick={this.handleClickGoToIM.bind(this)}
																		>
																			Submit
																		</Button>
																	</Grid>
																</Grid>
															</Grid>
														</CardBody>
													</Card>
												</Grid>
												<Grid item xs={12}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
													<Card
														data-background="color"
														data-color="white"
														style={{margin:'0px', backgroundColor:'#2B678C', paddingTop:'10px'}}
													>
														<CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
															<Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px', display: 'flex',marginTop:'	0px'}}>
																<Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>
																	<Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
																		<img style={{width:'100px'}}
																			alt="..."
																			onClick={handleClickGoToIP}
																			src={require("assets/img/ideabox/ip_round.png")}
																		/>
																	</Grid>
																	<Grid item xs={8}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
																		<h4 style={{
																			padding:'0px',
																			margin: '0px',
																			textAlign: 'left',
																			fontFamily: 'Montserrat',
																			fontWeight:'600',
																			fontSize: '15px',
																			color: '#ffffff',
																		}}>
																			Improvement Project
																		</h4>
																		<br/>
																		<h4 style={{
																			padding:'0px',
																			margin: '0px',
																			textAlign: 'left',
																			fontFamily: 'Montserrat',
																			fontWeight:'300',
																			fontSize: '12px',
																			color: '#ffffff',
																		}}>
																			Submit dan lakukan improvement projectmu sendiri atau bersama tim maksimal 2 project dalam satu waktu
																		</h4>
																		<Button
																			className="btn-round"
																			color="default"
																			type="button"
																			style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'100%', marginTop:'10px', marginBottom:'10px'}}
																			onClick={handleClickGoToIP}
																		>
																			Submit
																		</Button>
																	</Grid>
																</Grid>
															</Grid>
														</CardBody>
													</Card>
												</Grid>
												<Grid item xs={12}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
													<Card
														data-background="color"
														data-color="white"
														style={{margin:'0px', backgroundColor:'#2B678C', paddingTop:'10px'}}
													>
														<CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
															<Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px', display: 'flex',marginTop:'	0px'}}>
																<Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>
																	<Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
																		<img style={{width:'100px'}}
																			alt="..."

																			src={require("assets/img/ideabox/ss_round.png")}
																		/>
																	</Grid>
																	<Grid item xs={8}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
																		<h4 style={{
																			padding:'0px',
																			margin: '0px',
																			textAlign: 'left',
																			fontFamily: 'Montserrat',
																			fontWeight:'600',
																			fontSize: '15px',
																			color: '#ffffff',
																		}}>
																			Sumbang Saran
																		</h4>
																		<br/>
																		<h4 style={{
																			padding:'0px',
																			margin: '0px',
																			textAlign: 'left',
																			fontFamily: 'Montserrat',
																			fontWeight:'300',
																			fontSize: '12px',
																			color: '#ffffff',
																		}}>
																			Kami sedang mempersiapkan fitur ini. Sabar ya, kamu akan dapat mengakses fitur ini secepatnya.
																		</h4>
																		<Button
																			className="btn-round"
																			color="default"
																			type="button"
																			style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'100%', marginTop:'10px', marginBottom:'10px'}}

																		>
																			Cooming Soon
																		</Button>
																	</Grid>
																</Grid>
															</Grid>
														</CardBody>
													</Card>
												</Grid>
											</Grid>
										</Grid>

									</div>

	              </Container>

								<div style={{marginBottom:'100px'}}>
								</div>

	            </div>
	          </div>
	        </div>
	      </>
	    );
		}
  }

}

export default PresentationHeaderIdeation;
