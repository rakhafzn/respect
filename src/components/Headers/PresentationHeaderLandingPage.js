/*eslint-disable*/
import React from "react";

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Card, CardBody, Button, CardTitle, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			color: [],
			nextPage : null,
		};
	}

  componentDidMount() {
		const cookies = new Cookies()
    this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
  	})
	}

	handleClickLogin = () =>{
		// console.log(event.target.value)
		this.setState({
			nextPage : 'Login'
		})
	}

	handleClickHomepage = () =>{
		// console.log(event.target.value)
		const cookies = new Cookies();
		cookies.remove('userloggedin', {path: '/'});
		cookies.remove('login_status', {path: '/'});
		cookies.remove('user_email', {path: '/'});
		this.setState({
			nextPage : 'Homepage'
		})
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
    console.log(this.state.login_status)
    console.log(this.state.nextPage)

		if (this.state.nextPage !== null && this.state.nextPage === 'Login') {
			return(
				<Redirect from="*" to="/login" />
			)
		} else if (this.state.nextPage !== null && this.state.nextPage === 'Homepage') {
			return(
				<Redirect from="*" to="/home" />
			)
		} else {

	    return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'15px'}}>
	              <Container>
									<div className="title-brand">
										<img style={{width:'50%'}}
											alt="..."
											src={require("assets/img/logoideabox/Main_Logotype_white.png")}
										/>
										<h1 style={{fontSize: '18px', fontFamily: 'Montserrat', fontWeight:'600', marginTop:'40px'}}>
	                    <b>Selamat datang paragonian</b>
	                  </h1>

										<h1 style={{fontSize: '16px', fontFamily: 'Montserrat', fontWeight:'300', marginTop:'10px'}}>
	                    <b>Tagline lorem ipsum color der amet</b>
	                  </h1>

										<Card
											data-background="color"
											data-color="white"
											style={{margin:'15px'}}

										>
											<CardBody className="text-center" style={{padding:'5px', margin:'10px'}}>
												<h4 className="centerobject" style={{
													padding:'0px',
													margin: '0px',
													textAlign: 'center',
													fontFamily: 'Montserrat',
													fontWeight:'400',
													fontSize: '14px',
													color: '#2B678C',
													height:'30px'
												}}>
													Apakah kamu memiliki email kantor Paragon / Parama ?
												</h4>

												<Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px', display: 'flex',marginTop:'20px'}}>
							            <Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>
	     											<Grid item xs={6}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
															<Button
																style={{
																	width:'90%',
																	backgroundColor:"#F5B94F",
																	marginBottom:'10px',
																	borderRadius:'20px',
																	borderColor: '#ffffff',
																	color: '#ffffff'
																}}
														    onClick={this.handleClickLogin.bind(this)}
															>
																Iya
															</Button>
														</Grid>
	     											<Grid item xs={6}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
															<Button
																style={{
																	width:'90%',
																	backgroundColor:"rgba(0,0,0,0)",
																	marginBottom:'10px',
																	borderRadius:'20px',
																	borderColor: 'rgba(0,0,0,0.2)',
																	color: 'rgba(0,0,0,0.5)'
																}}
														    onClick={this.handleClickHomepage.bind(this)}
															>
																Tidak
															</Button>
														</Grid>
						         			</Grid>
					         			</Grid>
											</CardBody>
										</Card>

	                </div>

	              </Container>

	            </div>
	          </div>
	        </div>
	      </>
	    );
		}
  }

}

export default PresentationHeaderIdeation;
