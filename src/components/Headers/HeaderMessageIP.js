/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Divider from '@material-ui/core/Divider';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
		})
	}

	componentDidMount(){

	}

  render(){
		// #2B678C

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'30px'}}>
	              <Container>
									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
									<Row style={{width:'100%'}}>
	                  <Col className="ml-auto mr-auto">
											<Card className="card-register" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px', width:'90%'}}>
												<h4 className="centerobject" style={{
													padding:'0px',
													textAlign: 'left',
													margin: '5px',
													color: '#000000'
												}}>
													Pesan
												</h4>
											  <Divider />
												<div style={{marginBottom:'360px', paddingTop:'20px'}}>
													<p style={{color:'#000000'}}>
														Text Here
													</p>
												</div>
											</Card>
										</Col>
									</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

					<div style={{marginBottom:'100px'}}>
					</div>
	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
