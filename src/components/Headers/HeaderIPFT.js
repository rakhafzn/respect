/*eslint-disable*/
import React from "react";
import Datetime from "react-datetime";
import Moment from "moment";

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardHeader, CardTitle, Collapse, UncontrolledCollapse,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col, InputGroup, InputGroupAddon,
  InputGroupText } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			name: null,
			start_date: null,
			end_date: null,
			desc: null,
		})
	}

	componentDidMount(){
		axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?id=eq.2')
		.then(response => {
			console.log(response.data)

		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

	handleChangeName = event => {
		console.log(event.target.value)
		this.setState({
			name: event.target.value
		})
	}

	handleChangeStartDate = (moment, start_date) => {
		var startd = moment.toDate()
		console.log(Moment(startd).format("YYYY-MM-DD"))
		this.setState({
			start_date: Moment(startd).format("YYYY-MM-DD")
		})
	}

	handleChangeEndDate = (moment, end_date) => {
		var endd = moment.toDate()
		console.log(Moment(endd).format("YYYY-MM-DD"))
		this.setState({
			end_date: Moment(endd).format("YYYY-MM-DD")
		})
	}

	handleChangeJudulProject = event => {
		console.log(event.target.value)
		this.setState({
			desc: event.target.value
		})
	}

	handleSubmit = event =>{
		if(this.state.name == null || this.state.desc === null || this.state.start_date === null || this.state.end_date === null ) {
			console.log('KOMPLITIN DULU')
		} else {
			var idea_data = {
				"name": this.state.name,
				"desc": this.state.desc,
				"start_date": this.state.start_date,
				"end_date": this.state.end_date,
			}
			console.log(idea_data)
			// axios.post('https://training-api.pti-cosmetics.com/idea_submited_ip', idea_data)
			// .then(response => {
			// 	console.log('Idea berhasil di submit')
			// 	// hapus semua state
			// 	// refresh page ulang
			// 	// lainnya - > pop up
			// })
			// .catch(error => {
			// 	console.log(error)
			// 	console.log('GAGAL')
			// })
		}
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		// console.log(this.state.userloggedin)
		// console.log(this.state.login_status)

		console.log(this.state.name)
		console.log(this.state.desc)
		console.log(this.state.start_date)
		console.log(this.state.end_date)

		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			var today = new Date()
			var renderers = {
			  renderDay: function( props, currentDate, selectedDate ){
			    return <td {...props}>{currentDate.date() }</td>;
			  },
			  renderMonth: function( props, month, year, selectedDate){
			    return <td {...props}>{ month }</td>;
			  },
			  renderYear: function( props, year, selectedDate ){
			    return <td {...props}>{ year % 100 }</td>;
			  }
			}
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'80px'}}>
	              <Container>
									<h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px'}}>
										Hallo, {this.state.userloggedin.display_name}!
									</h2>
									<div className="title-brand">
	                  <h1 style={{fontSize: '18px', fontWeight:'400', fontFamily: 'Montserrat', marginTop:'15px'}}>
	                    <b>Tagline Improvement Project</b>
	                  </h1>
	                  <h1 style={{fontSize: '18px', fontWeight:'400',  fontFamily: 'Montserrat', marginTop:'0px'}}>
	                    <b>Lorem ipsum dolor sit emet</b>
	                  </h1>

										<div className="centerobject" style={{width:'100%'}}>
	                    <img
	                      className="centerobject fog-low"
	                      alt="..."
	                      src={require("assets/img/sections/fog-low.png")}
	                      style={{marginLeft:'0%', width:'100%', marginTop:'-70%'}}
	                    />
	                  </div>
	                </div>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
									<Row>
	                  <Col className="ml-auto mr-auto">
                      <Card className="card-reminder" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
                        <CardBody>
													<h5 className="mb-0 panel-title" style={{marginTop:'20px', fontWeight:'600', fontSize:'16px'}}>
														<i className="nc-icon nc-alert-circle-i" />
														<b>3 Tugas</b> deadline minggu ini
														<i className="nc-icon nc-minimal-right" />
													</h5>
												</CardBody>
                      </Card>

                      <h2 className="presentation-subtitle text-center" style={{marginTop:'20px', fontWeight:'600', fontSize:'16px'}}>
    	                	Form Tugas
    	                </h2>

                      <Card className="no-transition" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
                        <CardHeader className="card-collapse" id="headingOne" role="tab" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
                          <h5 className="mb-0 panel-title" style={{marginTop:'20px', fontWeight:'600', fontSize:'16px'}}>
                              Submit Tugas Project
                              <i className="nc-icon nc-minimal-down" />
                          </h5>
                        </CardHeader>
                        <UncontrolledCollapse toggler="#headingOne">
                          <CardBody>
                            <Form className="homeGrid">
                              <FormGroup>
                                <Input
                                  aria-describedby="emailHelp"
                                  id="exampleInputEmail1"
                                  placeholder="Tulis tugasmu di sini.."
                                  type="name"
                                  onChange={this.handleChangeName.bind(this)}
                                ></Input>
                              </FormGroup>
                              <FormGroup>
                                <Input
                                  id="exampleInputPassword1"
                                  placeholder="Deskripsi Tugas.."
                                  type="textarea"
                                  style={{ height: 100 }}
                                  onChange={this.handleChangeJudulProject.bind(this)}
                                ></Input>
                              </FormGroup>
                              <FormGroup>
                                <InputGroup className="date" id="datetimepicker1" position="relative">
                                  <Datetime
                                    inputProps={{
                                      className: "form-control",
                                      placeholder: "Start Date"
                                    }}
																		dateFormat="MMM D YYYY"
																		timeFormat={false}
																		onChange={this.handleChangeStartDate.bind(this)}
																		closeOnSelect="true"
                                  />
                                  <InputGroupAddon addonType="append">
                                    <InputGroupText>
                                      <span className="glyphicon glyphicon-calendar">
                                        <i className="fa fa-calendar" />
                                      </span>
                                    </InputGroupText>
                                  </InputGroupAddon>
                                </InputGroup>
                              </FormGroup>

                              <FormGroup>
                                <InputGroup className="date" id="datetimepicker2">
                                  <Datetime
                                    inputProps={{
                                      className: "form-control",
                                      placeholder: "End Date"
                                    }}
																		dateFormat="MMM D YYYY"
																		timeFormat={false}
																		onChange={this.handleChangeEndDate.bind(this)}
																		closeOnSelect="true"
                                  />
                                  <InputGroupAddon addonType="append">
                                    <InputGroupText>
                                      <span className="glyphicon glyphicon-calendar">
                                        <i className="fa fa-calendar" />
                                      </span>
                                    </InputGroupText>
                                  </InputGroupAddon>
                                </InputGroup>
                              </FormGroup>

                              <Button
                                className="btn-round"
                                color="default"
                                type="button"
                                style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'50%', marginTop:'10px'}}
                                onClick={this.handleSubmit.bind(this)}
                              >
                                Submit
                              </Button>
                            </Form>
                          </CardBody>
                        </UncontrolledCollapse>
                      </Card>

                      <h2 className="presentation-subtitle text-center" style={{marginTop:'20px', fontWeight:'600', fontSize:'16px'}}>
    	                	Timeline Project
    	                </h2>

											<Card className="card-timeline" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
												<CardBody style ={{backgroundColor:'#ffffff', margin:'5px', minHeight:'0px'}}>
													<Datetime
														input={false}
														timeFormat={false}
														renderDay={ renderers.renderDay }
														renderMonth={ renderers.renderMonth }
														renderYear={ renderers.renderYear }
														value={today}
													/>
												</CardBody>
											</Card>
										</Col>
									</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

					<div style={{marginBottom:'100px'}}>
					</div>
	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
