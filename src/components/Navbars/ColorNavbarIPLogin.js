import React from "react";
import { Link } from "react-router-dom";
import Cookies from 'universal-cookie'
// nodejs library that concatenates strings
import classnames from "classnames";
// JavaScript plugin that hides or shows a component based on your scroll
import Headroom from "headroom.js";
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import axios from 'axios'
// reactstrap components
import {
  Button,
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  NavbarBrand,
  Navbar,
  NavItem,
  Nav,
  Modal,
  Container,
  UncontrolledTooltip
} from "reactstrap";
// core components

function ColorNavbar() {
  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [bodyClick, setBodyClick] = React.useState(false);
  const [collapseOpen, setCollapseOpen] = React.useState(false);
  const [collapseMessageOpen, setCollapseMessageOpen] = React.useState(false);
  const [collapseActivityOpen, setcollapseActivityOpen] = React.useState(false);
  const [openDialogLogout, setOpenDialogLogout] = React.useState(false);

  React.useEffect(() => {
    let headroom = new Headroom(document.getElementById("navbar-main"));
    // initialise
    headroom.init();
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 499 ||
        document.body.scrollTop > 499
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 500 ||
        document.body.scrollTop < 500
      ) {
        setNavbarColor("navbar-transparent");
      }
    };
    window.addEventListener("scroll", updateNavbarColor);
    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };

  });

  function handleClickLogout(e) {
    // e.preventDefault();
    const cookies = new Cookies();
    cookies.remove('userloggedin', {path: '/'});
    cookies.remove('login_status', {path: '/'});
    cookies.remove('user_email', {path: '/'});
    console.log('The link was clicked.');
    console.log(e);
    setOpenDialogLogout(false);
    window.location.reload();
  }

  // #2B678C
  console.log('OPEN DIALOG LOGOUT')
  console.log(openDialogLogout)
  return (
    <>
      {bodyClick ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open");
            setBodyClick(false);
            setCollapseOpen(false);
          }}
        />
      ) : null}
      <Navbar
        className={classnames("fixed-top", navbarColor)}
        expand="lg"
        id="navbar-main"
        style={{paddingTop:'0px',backgroundColor:'#2B678C'}}
      >
        <Container style={{backgroundColor:'#ffffff'}}>
          <div className="navbar-translate">
            <NavbarBrand id="navbar-brand" to="/login" tag={Link} style={{paddingBottom:'0px'}}>
              <img style={{width:'20%'}}
                alt="..."
                src={require("assets/img/logoideabox/Main_Logotype.png")}
              />
            </NavbarBrand>
          </div>
        </Container>
      </Navbar>
    </>
  );
}

export default ColorNavbar;
