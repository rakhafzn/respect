/*!

=========================================================
* Argon Dashboard PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// react plugin that prints a given react component
//import ReactToPrint from "react-to-print";
import { Link } from "react-router-dom";
// react component for creating dynamic tables
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
import Cookies from 'universal-cookie'

import axios from "axios";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  Container,
  Row,
  Col,
} from "reactstrap";
// core components
import SimpleHeader from "views/dashboard/SimpleHeader.js";

const pagination = paginationFactory({
  page: 1,
  alwaysShowAllBtns: true,
  showTotal: true,
  withFirstAndLast: false,
  sizePerPageRenderer: ({ options, currSizePerPage, onSizePerPageChange }) => (
    <div className="dataTables_length" id="datatable-basic_length">
      <label>
        Show{" "}
        {
          <select
            name="datatable-basic_length"
            aria-controls="datatable-basic"
            className="form-control form-control-sm"
            onChange={e => onSizePerPageChange(e.target.value)}
          >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        }{" "}
        entries.
      </label>
    </div>
  )
});

const { SearchBar } = Search;

class ReactBSTables extends React.Component {

  constructor(props) {
    super(props)
    this.GetActionFormat = this.GetActionFormat.bind(this)
  }
  state = {
    alert: null,
    dataShopper: [],
  };

  componentDidMount(){
    console.log('DID MOUNT')
    axios.get('https://training-api.pti-cosmetics.com/v_datashopper')
    .then(response => {
      console.log(response.data)
      this.setState({
        dataShopper: response.data
      })
    })
    .catch(error => {
      console.log(error)
    })
  }

  onPartnerClick = (cell, partner, rowIndex) => {
    console.log(cell)
    console.log(partner)
    console.log(rowIndex)
    const cookies = new Cookies();
    cookies.set('selectedPartner',partner,{path: '/'});
  }

  buttonAction (cell, row, enumObject, rowIndex) {
    console.log('CLICKED')
    return(
      <Button
        className="buttons-copy buttons-html5"
        color="default"
        size="sm"
        id="copy-tooltip"
        onClick={this.onPartnerClick(cell,row,rowIndex)}
      >
        <span>Action</span>
      </Button>
    )
  }

  GetActionFormat(cell, row) {
    return (
        <div>
          <Link to="/admin/dashboard-partner">
            <button
              type="button"
              className="btn btn-outline-primary btn-sm ts-buttom"
              size="sm"
              onClick={this.onPartnerClick.bind(this,cell,row)}
            >
              View
            </button>
          </Link>
        </div>
    );
  }
  // this function will copy to clipboard an entire table,
  // so you can paste it inside an excel or csv file
  copyToClipboardAsTable = el => {
    var body = document.body,
      range,
      sel;
    if (document.createRange && window.getSelection) {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      try {
        range.selectNodeContents(el);
        sel.addRange(range);
      } catch (e) {
        range.selectNode(el);
        sel.addRange(range);
      }
      document.execCommand("copy");
    } else if (body.createTextRange) {
      range = body.createTextRange();
      range.moveToElementText(el);
      range.select();
      range.execCommand("Copy");
    }
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Good job!"
          onConfirm={() => this.setState({ alert: null })}
          onCancel={() => this.setState({ alert: null })}
          confirmBtnBsStyle="info"
          btnSize=""
        >
          Copied to clipboard!
        </ReactBSAlert>
      )
    });
  };
  render() {
    const buttonAction = this.buttonAction.bind(this)
    console.log('data shopper')
    return (
      <>
        {this.state.alert}
        <SimpleHeader name="Database Shopper" parentName="Tables" />
        <Container className="mt--6" fluid>
          <Row>
            <div className="col">
              <Card>
                <CardHeader>
                  <h3 className="mb-0">Details Toko</h3>
                </CardHeader>
                <ToolkitProvider
                  data={this.state.dataShopper}
                  keyField="name"
                  columns={[
                    {
                      dataField: "id",
                      text: "ID",
                      sort: true
                    },
                    {
                      dataField: "cust_name",
                      text: "Nama",
                      sort: true
                    },
                    {
                      dataField: "gender",
                      text: "Gender",
                      sort: true
                    },
                    {
                      dataField: "age",
                      text: "Age",
                      sort: true
                    },
                    {
                      dataField: "job",
                      text: "Job",
                      sort: true
                    },
                    {
                      dataField: "phone",
                      text: "Phone",
                      sort: true
                    },
                    {
                      dataField: "province",
                      text: "Provinsi",
                      sort: true
                    },
                    {
                      dataField: "city",
                      text: "Kota",
                      sort: true
                    },
                    {
                      dataField: "subdistrict",
                      text: "Kecamatan",
                      sort: true
                    },
                    {
                      dataField: "address",
                      text: "Alamat",
                      sort: true
                    },
                    {
                      dataField: "partner)id",
                      text: "Partner ID",
                      sort: true
                    },
                    {
                      dataField: "partner_name",
                      text: "Toko",
                      sort: true
                    },
                    {
                      dataField: "brand",
                      text: "Brand",
                      sort: true
                    },
                    // {
                    //   text: "Action",
                    //   dataField: "",
                    //   formatter:this.GetActionFormat,
                    // }
                  ]}
                  search
                >
                  {props => (
                    <div className="py-4 table-responsive">
                      <div
                        id="datatable-basic_filter"
                        className="dataTables_filter px-4 pb-1"
                      >
                        <label>
                          Search:
                          <SearchBar
                            className="form-control-sm"
                            placeholder=""
                            {...props.searchProps}
                          />
                        </label>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        bootstrap4={true}
                        pagination={pagination}
                        bordered={false}
                        onClick={this.onPartnerClick.bind(this)}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </Card>
              {/*
              <Card>
                <CardHeader>
                  <h3 className="mb-0">Action buttons</h3>
                  <p className="text-sm mb-0">
                    This is an exmaple of data table using the well known
                    react-bootstrap-table2 plugin. This is a minimal setup in
                    order to get started fast.
                  </p>
                </CardHeader>
                <ToolkitProvider
                  data={this.state.dataPartner}
                  keyField="name"
                  columns={[
                    {
                      dataField: "partner_id",
                      text: "ID",
                      sort: true
                    },
                    {
                      dataField: "partner_name",
                      text: "Nama Store",
                      sort: true
                    },
                    {
                      dataField: "partner_ref",
                      text: "Kode",
                      sort: true
                    },
                    {
                      dataField: "address",
                      text: "Address",
                      sort: true
                    },
                    {
                      dataField: "city",
                      text: "City",
                      sort: true
                    },
                    {
                      dataField: "dc",
                      text: "DC",
                      sort: true
                    },
                    {
                      dataField: "postcode",
                      text: "Post",
                      sort: true
                    },
                    {
                      dataField: "price_type",
                      text: "Type",
                      sort: true
                    },
                    {
                      dataField: "jml_ba",
                      text: "Jumlah BA",
                      sort: true
                    },
                    {
                      text: "Action",
                      dataField: "",
                      formatter:this.GetActionFormat,
                    }
                  ]}
                  search
                >
                  {props => (
                    <div className="py-4 table-responsive">
                      <Container fluid>
                        <Row>
                          <Col xs={12} sm={6}>
                            <ButtonGroup>
                              <Button
                                className="buttons-copy buttons-html5"
                                color="default"
                                size="sm"
                                id="copy-tooltip"
                                onClick={() =>
                                  this.copyToClipboardAsTable(
                                    document.getElementById("react-bs-table")
                                  )
                                }
                              >
                                <span>Copy</span>
                              </Button>
                              <ReactToPrint.default
                                trigger={() => (
                                  <Button
                                    color="default"
                                    size="sm"
                                    className="buttons-copy buttons-html5"
                                    id="print-tooltip"
                                  >
                                    Print
                                  </Button>
                                )}
                                content={() => this.componentRef}
                              />
                            </ButtonGroup>
                            <UncontrolledTooltip
                              placement="top"
                              target="print-tooltip"
                            >
                              This will open a print page with the visible rows
                              of the table.
                            </UncontrolledTooltip>
                            <UncontrolledTooltip
                              placement="top"
                              target="copy-tooltip"
                            >
                              This will copy to your clipboard the visible rows
                              of the table.
                            </UncontrolledTooltip>
                          </Col>
                          <Col xs={12} sm={6}>
                            <div
                              id="datatable-basic_filter"
                              className="dataTables_filter px-4 pb-1 float-right"
                            >
                              <label>
                                Search:
                                <SearchBar
                                  className="form-control-sm"
                                  placeholder=""
                                  {...props.searchProps}
                                />
                              </label>
                            </div>
                          </Col>
                        </Row>
                      </Container>
                      <BootstrapTable
                        ref={el => (this.componentRef = el)}
                        {...props.baseProps}
                        bootstrap4={true}
                        pagination={pagination}
                        bordered={false}
                        id="react-bs-table"
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </Card>*/}
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default ReactBSTables;
