import React from "react";
// reactstrap components

import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';

import {  UncontrolledCollapse, Button, Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";
// core components

// style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}

// function SectionCardsIdeation() {
export class SectionCardsIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			color: [],
			description_popup : false,
			choosed_detail : null,
		};
	}

  componentDidMount() {
		this.setState({
			// userloggedin: this.props.userloggedin,
			// color: this.props.color,
		})
	}

	handleClickPopUpDescription = (event,target) =>{
		console.log(event);
		// console.log(target);
    this.setState({
      description_popup : true,
			choosed_detail : event,
    });
	}

	closeModal = () => {
		this.setState({
			description_popup : false,
		})
	}

	render(){
		const choosed_detail = this.state.choosed_detail
		// console.log(this.state.BM_popup)

		// #2B678C
	  return (
	    <>

				<div className="section" style ={{backgroundColor:'rgba(0,0,0,0)', paddingTop:'0px', paddingBottom:'0px'}}>
					<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
						<Container>
							<div className="title-brand">
								<Grid container justify="center" style={{ padding:'0px', margin:'0px'}}>

				          <Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px', display: 'flex',}}>
				            <Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>

				              <Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
				                <Card
				                  data-background="color"
				                  data-color="white"
				                  style={{margin:'0px'}}
													onClick={this.handleClickPopUpDescription.bind(this, 'BM')}
				                >
				                  <CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
				                    <img style={{width:'100px'}}
				                      alt="..."
				                      src={require("assets/img/Icon/Icons/BM.png")}
				                    />
				                    <h4 className="centerobject" style={{
				                      padding:'0px',
				                      margin: '0px',
				                      textAlign: 'center',
				                      fontFamily: 'Montserrat',
				                      fontWeight:'600',
				                      fontSize: '10px',
				                      color: '#000000',
				                      height:'30px'
				                    }}>
				                      Business Model
				                    </h4>
				                  </CardBody>
				                </Card>
				              </Grid>

				              <Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
												<Card
													data-background="color"
													data-color="white"
													style={{margin:'0px'}}
													onClick={this.handleClickPopUpDescription.bind(this, 'BP')}
												>
												  <CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
				                    <img style={{width:'100px'}}
				                      alt="..."
				                      src={require("assets/img/Icon/Icons/BP.png")}
				                    />
				                    <h4 className="centerobject" style={{
				                      padding:'0px',
				                      margin: '0px',
				                      textAlign: 'center',
				                      fontFamily: 'Montserrat',
				                      fontWeight:'600',
				                      fontSize: '10px',
				                      color: '#000000',
				                      height:'30px'
				                    }}>
				                      Business Process
				                    </h4>
				                  </CardBody>
				                </Card>
				              </Grid>

				              <Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
												<Card
													data-background="color"
													data-color="white"
													style={{margin:'0px'}}
													onClick={this.handleClickPopUpDescription.bind(this, 'BPM')}
												>
												  <CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
				                    <img style={{width:'100px'}}
				                      alt="..."
				                      src={require("assets/img/Icon/Icons/BPM.png")}
				                    />
				                    <h4 className="centerobject" style={{
				                      padding:'0px',
				                      margin: '0px',
				                      textAlign: 'center',
				                      fontFamily: 'Montserrat',
				                      fontWeight:'600',
				                      fontSize: '10px',
				                      color: '#000000',
				                      height:'30px'
				                    }}>
				                      Brand and Product Management
				                    </h4>
				                  </CardBody>
				                </Card>
				              </Grid>

				            </Grid>
				          </Grid>

				          <Grid item xs={12}  style={{ padding:'0px', marginLeft:'0px', marginRight:'0px'}}>
				            <Grid container justify="center" className="homeGrid" style={{ padding:'0px', margin:'0px'}}>

				              <Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
												<Card
													data-background="color"
													data-color="white"
													style={{margin:'0px'}}
													onClick={this.handleClickPopUpDescription.bind(this, 'CEW')}
												>
												  <CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
				                    <img style={{width:'100px'}}
				                      alt="..."
				                      src={require("assets/img/Icon/Icons/CEW.png")}
				                    />
				                    <h4 className="centerobject" style={{
				                      padding:'0px',
				                      margin: '0px',
				                      textAlign: 'center',
				                      fontFamily: 'Montserrat',
				                      fontWeight:'600',
				                      fontSize: '10px',
				                      color: '#000000',
				                      height:'30px'
				                    }}>
				                      Customer Engagement Way
				                    </h4>
				                  </CardBody>
				                </Card>
				              </Grid>

				              <Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
												<Card
													data-background="color"
													data-color="white"
													style={{margin:'0px'}}
													onClick={this.handleClickPopUpDescription.bind(this, 'SPD')}
												>
												  <CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
				                    <img style={{width:'100px'}}
				                      alt="..."
				                      src={require("assets/img/Icon/Icons/SPD.png")}
				                    />
				                    <h4 className="centerobject" style={{
				                      padding:'0px',
				                      margin: '0px',
				                      textAlign: 'center',
				                      fontFamily: 'Montserrat',
				                      fontWeight:'600',
				                      fontSize: '10px',
				                      color: '#000000',
				                      height:'30px'
				                    }}>
				                      Sales and Product Delivery
				                    </h4>
				                  </CardBody>
				                </Card>
				              </Grid>

				              <Grid item xs={4}  style={{ padding:'5px', marginLeft:'0px', marginRight:'0px'}}>
												<Card
													data-background="color"
													data-color="white"
													style={{margin:'0px'}}
													onClick={this.handleClickPopUpDescription.bind(this, 'WOW')}
												>
												  <CardBody className="text-center" style={{padding:'0px', margin:'10px'}}>
				                    <img style={{width:'100px'}}
				                      alt="..."
				                      src={require("assets/img/Icon/Icons/WOW.png")}
				                    />
				                    <h4 className="centerobject" style={{
				                      padding:'0px',
				                      margin: '0px',
				                      textAlign: 'center',
				                      fontFamily: 'Montserrat',
				                      fontWeight:'600',
				                      fontSize: '10px',
				                      color: '#000000',
				                      height:'30px'
				                    }}>
				                      Ways of working
				                    </h4>
				                  </CardBody>
				                </Card>
				              </Grid>

				            </Grid>
				          </Grid>
				        </Grid>
							</div>
						</Container>
					</div>
	      </div>

				<Dialog
					open={this.state.description_popup}
					closeOnDocumentClick
					onClose={this.closeModal}
				>
					{(function(){
						if(choosed_detail === 'BM'){
							return(
								<div>
									<h4 className="centerobject" style={{
										padding:'5px',
										textAlign: 'center',
										margin: '5px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										fontSize: '20px',
									}}>
										Business Model
									</h4>
									<div className='centerobject'>
										<img
											style={{width:'50%', marginLeft:'25%'}}
											alt="..."
											src={require("assets/img/Icon/Icons/BM.png")}
										/>
									</div>
									<p className="centerobject" style={{
										paddingLeft:'20px',
										paddingTop:'10px',
										paddingRight:'20px',
										textAlign: 'center',
										fontWeight:'400'
									}}>
										How we create and deliver value in order to achieve company vision and mission
									</p>
								</div>
							)
						} else if(choosed_detail === 'BP'){
							return(
								<div>
									<h4 className="centerobject" style={{
										padding:'5px',
										textAlign: 'center',
										margin: '5px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										fontSize: '20px',
									}}>
										Business Process
									</h4>
									<div className='centerobject'>
										<img
											style={{width:'50%', marginLeft:'25%'}}
											alt="..."
											src={require("assets/img/Icon/Icons/BP.png")}
										/>
									</div>
									<p className="centerobject" style={{
										paddingLeft:'20px',
										paddingTop:'10px',
										paddingRight:'20px',
										textAlign: 'center',
										fontWeight:'400'
									}}>
										How we run the process to create output(s) or product(s) from one or several input(s) effectively and efficiently.
									</p>
								</div>
							)
						} else if(choosed_detail === 'BPM'){
							return(
								<div>
									<h4 className="centerobject" style={{
										padding:'5px',
										textAlign: 'center',
										margin: '5px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										fontSize: '20px',
									}}>
										Brand and Product Management
									</h4>
									<div className='centerobject'>
										<img
											style={{width:'50%', marginLeft:'25%'}}
											alt="..."
											src={require("assets/img/Icon/Icons/BPM.png")}
										/>
									</div>
									<p className="centerobject" style={{
										paddingLeft:'20px',
										paddingTop:'10px',
										paddingRight:'20px',
										textAlign: 'center',
										fontWeight:'400'
									}}>
										How we maintain our business by managing our brands and products portfolio: new and existing.
									</p>
								</div>
							)
						} else if(choosed_detail === 'CEW'){
							return(
								<div>
									<h4 className="centerobject" style={{
										padding:'5px',
										textAlign: 'center',
										margin: '5px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										fontSize: '20px',
									}}>
										Customer Engagement Way
									</h4>
									<div className='centerobject'>
										<img
											style={{width:'50%', marginLeft:'25%'}}
											alt="..."
											src={require("assets/img/Icon/Icons/CEW.png")}
										/>
									</div>
									<p className="centerobject" style={{
										paddingLeft:'20px',
										paddingTop:'10px',
										paddingRight:'20px',
										textAlign: 'center',
										fontWeight:'400'
									}}>
										How we maintain relationship and loyalty of our customers.
									</p>
								</div>
							)
						} else if(choosed_detail === 'SPD'){
							return(
								<div>
									<h4 className="centerobject" style={{
										padding:'5px',
										textAlign: 'center',
										margin: '5px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										fontSize: '20px',
									}}>
										Sales Product Delivery
									</h4>
									<div className='centerobject'>
										<img
											style={{width:'50%', marginLeft:'25%'}}
											alt="..."
											src={require("assets/img/Icon/Icons/SPD.png")}
										/>
									</div>
									<p className="centerobject" style={{
										paddingLeft:'20px',
										paddingTop:'10px',
										paddingRight:'20px',
										textAlign: 'center',
										fontWeight:'400'
									}}>
										How we boost our company sales and improve our ways to deliver our products to customers.
									</p>
								</div>
							)
						} else if(choosed_detail === 'WOW'){
							return(
								<div>
									<h4 className="centerobject" style={{
										padding:'5px',
										textAlign: 'center',
										margin: '5px',
										fontFamily: 'Montserrat',
										fontWeight:'600',
										fontSize: '20px',
									}}>
										Ways of Working
									</h4>
									<div className='centerobject'>
										<img
											style={{width:'50%', marginLeft:'25%'}}
											alt="..."
											src={require("assets/img/Icon/Icons/WOW.png")}
										/>
									</div>
									<p className="centerobject" style={{
										paddingLeft:'20px',
										paddingTop:'10px',
										paddingRight:'20px',
										textAlign: 'center',
										fontWeight:'400'
									}}>
										The method or mechanism we use in our job that will boost our productivity despite current condition.
									</p>
								</div>
							)
						}
					})()}

					<div className="centerobject" style={{widht:'100%', textAlign: 'center', marginTop:'10px'}}>
						<Button
							className="centerobject btn-round"
							color="default"
							type="button"
							style={{backgroundColor:'#2B678C', borderColor:'#2B678C', width:'50%', marginBottom:'10px'}}
							onClick={this.closeModal.bind(this)}
						>
							OK
						</Button>
					</div>

				</Dialog>

	    </>
	  );
	}
}

export default SectionCardsIdeation;
