/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';
import Datetime from "react-datetime";
import Select from "react-select";
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import Swal from 'sweetalert2';
import Moment from "moment";
import Icon from '@material-ui/core/Icon';

// reactstrap components
import axios from 'axios'
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			user_role: cookies.get('user_role'),
			name: null,
			directorate: null,
			judulProject: null,
			idea_submitted : [],
      		idea_filtered : [],
			idea_choosen : [],
			description_popup : false,
			choosed_detail : null,
			status_urs : null,
			comment : null,
			filename_download : null,
			itbp: null,
			ithead: null,
			developer1: null,
			developer2: null,
			developer3: null,
			bp_field: false,
			head_field: false,
			dev_field: false,
			comment_field: false,
			bp_text: false,
			head_text: false,
			dev_text: false,
			comment_text: false,
			list_bp: [],
			list_head: [],
			list_dev: [],
			status_field: true,
		})
	}

	componentDidMount(){
		// console.log(this.state.user_email)
		console.log('Sedang ambil data...')
		var role = 'business_owner=eq.anakbawang123-comcom';
		if(this.state.user_role === 'BUSINESS PARTNER'){
			this.setState({
				bp_text: true,
				head_text: false,
				dev_text: false,
				status_field: true,
			})
			role = 'status=eq.approved1&business_partner=eq.'+this.state.user_email;
		} else if(this.state.user_role === 'BUSINESS OWNER'){
			this.setState({
				bp_text: false,
				head_text: false,
				dev_text: false,
				status_field: true,
			})
			role = 'status=eq.request&business_owner=eq.'+this.state.user_email;
		} else if(this.state.user_role === 'IT HEAD'){
			this.setState({
				bp_text: true,
				head_text: true,
				dev_text: false,
				status_field: true,
			})
			role = 'status=eq.approved2&it_head=eq.'+this.state.user_email;
		} else if(this.state.user_role === 'DEVELOPER'){
			this.setState({
				bp_text: true,
				head_text: true,
				dev_text: true,
				status_field: false,
			})
			role = 'status=eq.approved3&or=(developer1.eq.'+this.state.user_email+',developer2.eq.'+this.state.user_email+',developer3.eq.'+this.state.user_email+')';
		}
		console.log(this.state.user_role)
		console.log(role)
		axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_form?'+role)
		.then(response => {
      function filterArray(array, pname) {
        return array.filter ((element) => {
          return element.requester_name === pname
        })
      }
      const users = [...new Set(response.data.map(x => x.requester_name))]
      console.log(response.data)
      console.log(users)
      var pname2 = users[0]
      console.log(filterArray(response.data, pname2))
			this.setState({
				idea_submitted : response.data,
        idea_filtered : filterArray(response.data, pname2),
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
		axios.get('https://sfa-api-testing.pti-cosmetics.com/respect_user?role=neq.null')
		.then(response => {
			console.log(response.data)
			function filterArray(array, urole) {
				return array.filter ((element) => {
					return element.role === urole
				})
			}
			console.log(filterArray(response.data, 'BUSINESS PARTNER'))
			console.log(filterArray(response.data, 'IT HEAD'))
			console.log(filterArray(response.data, 'DEVELOPER'))
			this.setState({
				list_bp: filterArray(response.data, 'BUSINESS PARTNER'),
				list_head: filterArray(response.data, 'BUSINESS PARTNER'),
				list_dev: filterArray(response.data, 'DEVELOPER')
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

	handleChangeStatus = event => {
		// console.log(event.target.value)
		if (event.target.value === 'Approve') {
			if (this.state.user_role === 'BUSINESS OWNER') {
				console.log('Munculkan dropdown BP')
				this.setState({
					bp_field: true,
					head_field: false,
					dev_field: false,
					comment_field: false
				})
			} else if (this.state.user_role === 'BUSINESS PARTNER') {
				console.log('Munculkan dropdown IT Head')
				this.setState({
					bp_field: false,
					head_field: true,
					dev_field: false,
					comment_field: false
				})
			} else if (this.state.user_role === 'IT HEAD') {
				console.log('Munculkan dropdown Developer')
				this.setState({
					bp_field: false,
					head_field: false,
					dev_field: true,
					comment_field: false
				})
			} else if (this.state.user_role === 'Developer') {
				console.log('Munculkan text field Risk and Impact')
				this.setState({
					bp_field: false,
					head_field: false,
					dev_field: false,
					comment_field: false
				})
			} else {
				console.log('Seharusnya tidak bisa review')
			}
		} else {
			console.log('Munculkan text area comment')
			this.setState({
				bp_field: false,
				head_field: false,
				dev_field: false,
				comment_field: true
			})
		}
		this.setState({
			status_urs: event.target.value
		})
	}
	handleChangeComment = event => {
		// console.log(event.target.value)
		this.setState({
			comment: event.target.value
		})
	}
	handleChangeBusinessPartner = event => {
		// console.log(event.target.value)
		this.setState({
			itbp: event.target.value
		})
	}
	handleChangeItHead = event => {
		// console.log(event.target.value)
		this.setState({
			ithead: event.target.value
		})
	}
	handleChangeDeveloper1 = event => {
		// console.log(event.target.value)
		this.setState({
			developer1: event.target.value
		})
	}
	handleChangeDeveloper2 = event => {
		// console.log(event.target.value)
		this.setState({
			developer2: event.target.value
		})
	}
	handleChangeDeveloper3 = event => {
		// console.log(event.target.value)
		this.setState({
			developer3: event.target.value
		})
	}

  filterProject = event => {
		// console.log(event)
    function filterArray(array, pname) {
      return array.filter ((element) => {
        return element.requester_name === pname
      })
    }
    // var participantName = event.target.textContent
    var listtask = filterArray(this.state.idea_submitted, event)
    this.setState({
      idea_filtered : listtask,
    })
  }

	handleClickChooseFileDownload = (event,obj) => {
		// console.log(event)
		// console.log(obj)
		this.setState({
			filename_download : event
		})
	}

	handleClickPopUpDescription = (event,target) =>{
		console.log(event)
		function filterArray(array, project_name) {
      return array.filter ((element) => {
        return element.project_name === project_name
      })
    }
    // var participantName = event.target.textContent
    var listtask = filterArray(this.state.idea_submitted, event)
    console.log(listtask)
		// console.log(target);
    this.setState({
      description_popup : true,
			choosed_detail : event,
			idea_choosen : listtask,
    });
	}

	handleSubmit = event =>{
		console.log(event)
		console.log(event.project_name)
		if(this.state.status_urs === null || this.state.status_urs === "" || this.state.status_urs === "Pilih Status..." 
		|| (this.state.user_role === 'BUSINESS OWNER' && (this.state.itbp === null || this.state.itbp === ""))
		|| (this.state.user_role === 'BUSINESS PARTNER' && (this.state.it_head === null || this.state.it_head === ""))
		|| (this.state.user_role === 'IT HEAD' && ((this.state.developer1 === null || this.state.developer1 === "")
		&& (this.state.developer2 === null || this.state.developer2 === "") 
		&& (this.state.developer3 === null || this.state.developer3 === ""))) 
		|| (this.state.status_urs === 'Decline' && (this.comment === null || this.state.comment === ""))) {
			Swal.fire({
				icon: 'error',
				text: 'Pilih status approval dan lengkapi form sebelum update!',
				confirmButtonColor: '#e55555',
			});
		} else {
			const status_urs_project = this.state.status_urs
			const userrole = this.state.user_role
			var task = '-'
			var task_label = '-'
			if (status_urs_project === 'Approve') {
				if (userrole === 'BUSINESS OWNER') {
					task = 'approved1'
					task_label = 'Approved'
				} else if (userrole === 'BUSINESS PARTNER') {
					task = 'approved2'
					task_label = 'Approved'
				} else if (userrole === 'IT HEAD') {
					task = 'approved3'
					task_label = 'Approved'
				} else {
					task = 'developed'
					task_label = 'Approved'
				}
			} else if (status_urs_project === 'Decline') {
				task = 'rejected'
				task_label = 'Rejected'
			}
			var idea_data = {
				"status": task,
				"business_partner": this.state.itbp,
				"it_head": this.state.it_head,
				"developer1": this.state.developer1,
				"developer2": this.state.developer2,
				"developer3": this.state.developer3,
				"comment": this.state.comment,
			}
			var log_data = {
				"email": event.requester_email,
				"urs": event.project_name,
				"action": task,
				"reference": this.state.idea_choosen[0].ref
			}
			console.log(idea_data)
			console.log(log_data)
			axios.patch('https://sfa-api-testing.pti-cosmetics.com/urs_form?id=eq.'+event.id, idea_data)
			.then(response => {
				axios.post('https://sfa-api-testing.pti-cosmetics.com/urs_log', log_data).then(response=>{
					
				})
				console.log('Idea berhasil di approve')
				Swal.fire({
				icon: 'success',
				text: 'The URS has been '+task_label,
				confirmButtonColor: 'blue',
	        }).then(function (result) {
					  if (true) {
					    window.location = "/review-details";
					  }
					});
				})
			.catch(error => {
				console.log(error)
				console.log('GAGAL')
			})
		}
		this.setState({
			description_popup : false,
		})
	}

	closeModal = () => {
		this.setState({
			description_popup : false,
			bp_field: false,
			head_field: false,
			dev_field: false,
			comment_field: false,
		})
	}

	render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
		console.log(this.state.login_status)
		console.log(this.state.user_role)
		console.log(this.state.name)
		console.log(this.state.directorate)
		console.log(this.state.judulProject)
		console.log(this.state.status_urs)
		console.log(this.state.comment)
		SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/home" />
      )
    } else {
			var openfile = 'https://testing-bivi-007.pti-cosmetics.com/bivi-api/assets/'+this.state.filename_download
			console.log(openfile)
			const handleClickChooseFileDownload = this.handleClickChooseFileDownload
			const choosed_detail = this.state.choosed_detail
			const handleSubmit = this.handleSubmit.bind(this)
			const handleStatusChange = this.handleChangeStatus.bind(this)
			const handleCommentChange = this.handleChangeComment.bind(this)
			const handleBusinessPartnerChange = this.handleChangeBusinessPartner.bind(this)
			const handleItHeadChange = this.handleChangeItHead.bind(this)
			const handleDeveloper1Change = this.handleChangeDeveloper1.bind(this)
			const handleDeveloper2Change = this.handleChangeDeveloper2.bind(this)
			const handleDeveloper3Change = this.handleChangeDeveloper3.bind(this)
			const list_bp = this.state.list_bp
			const list_head = this.state.list_head
			const list_dev = this.state.list_dev
			var chosen_task = this.state.idea_choosen[0]
			var idea_submitter_name = null
			var idea_id = null
			var project_desc = null
			var priority = null
			var entity = null
			var impacted_apps = null
			var business_owner = null
			var dir = null
			var golive_date = null
			var idea_date = null
			var golive_date_formatted = null
			var idea_file = null
			var idea_timestamp = null
			var background_urs = null
			var objective = null
			var scope = null
			var deliverable = null
			var current_state = null
			var desired_state = null
			var quantitative = null
			var qualitative = null
			var risk_assumption = null
			var succes_matrix = null
			var uat_scenario = null
			var bp_field = false
			var head_field = false
			var dev_field = false
			var bp_text = false
			var head_text = false
			var dev_text = false
			var comment_field = false
      var today = new Date()
			var renderers = {
			  renderDay: function( props, currentDate, selectedDate ){
			    return <td {...props}>{currentDate.date() }</td>;
			  },
			  renderMonth: function( props, month, year, selectedDate){
			    return <td {...props}>{ month }</td>;
			  },
			  renderYear: function( props, year, selectedDate ){
			    return <td {...props}>{ year % 100 }</td>;
			  }
			}
      var listproject = this.state.idea_submitted
      const users = [...new Set(listproject.map(x => x.requester_name))]
      var listproject2 = this.state.idea_filtered
      const titles2 = [...new Set(listproject2.map(x => x.project_name))]
      console.log(users)
      console.log(users[0])
      console.log(titles2)
			console.log(chosen_task)
			console.log(this.state.idea_choosen)
			console.log(this.state.choosed_detail)
			if (!chosen_task) {
				console.log("masih kosong")
			} else {
				idea_submitter_name = chosen_task.name
				idea_id = chosen_task.id
				project_desc = chosen_task.project_desc
				priority = chosen_task.priority
				entity = chosen_task.entity
				impacted_apps = chosen_task.impacted_apps
				golive_date = chosen_task.golive_date
				business_owner = chosen_task.business_owner
				dir = chosen_task.directorate
				idea_timestamp = chosen_task.write_date
				idea_file = chosen_task.filename
				background_urs = chosen_task.background
				objective = chosen_task.objective
				scope = chosen_task.scope
				deliverable = chosen_task.deliverable
				current_state = chosen_task.current_state
				desired_state = chosen_task.desired_state
				quantitative = chosen_task.quantitative_benefit
				qualitative = chosen_task.qualitative_benefit
				risk_assumption = chosen_task.risk_assumption
				succes_matrix = chosen_task.success_matrix
				uat_scenario = chosen_task.uat_scenario
				idea_date = Moment(idea_timestamp).format("D MMM YYYY")
				golive_date_formatted = Moment(golive_date).format("D MMM YYYY")
				bp_field = this.state.bp_field
				head_field = this.state.head_field
				dev_field = this.state.dev_field
				comment_field = this.state.comment_field
				bp_text = this.state.bp_text
				head_text = this.state.head_text
				dev_text = this.state.dev_text
				console.log(golive_date_formatted)
				console.log(idea_date)
			}
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', padding:'0px', marginBottom:'20px', marginTop:'120px'}}
	            className="section"
	          >
	            <div style={{marginTop:'40px'}}>
	              <Container>
								<h2 className="presentation-subtitle text-center" style={{marginTop:'20px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
									Requester
								</h2>
								<Swiper
						      spaceBetween={7}
						      slidesPerView={3}
									pagination={{clickable:true}}
						      onSlideChange={() => console.log('slide change')}
						      onSwiper={(swiper) => console.log(swiper)}
									style={{margin:'0px',padding:'5px'}}
						    >
								{users.map((row)=> (
									<SwiperSlide>
										<Card
											data-background="color"
											data-color="white"
											style={{margin:'0px', backgroundColor:'#ffffff',
											color:'#333333',
											boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.05), 0 1px 5px 0 rgba(0, 0, 0, 0.05)',}}
											onClick={this.filterProject.bind(this, row)}
										>
											<CardBody className="text-center" style={{padding:'5px', margin:'10px'}}>
												<img style={{width:'30px', margin:'3px'}}
													alt="..."
													src={require("assets/img/Icon/Icons/Participant.png")}
												/>
												<h4 className="centerobject" style={{
													padding:'0px',
													margin: '10px',
													textAlign: 'center',
													fontFamily: 'Montserrat',
													fontWeight:'600',
													fontSize: '12px',
													color:'#2B678C',
													height:'30px'
												}}>
													{row}
												</h4>
											</CardBody>
										</Card>
										<h3 className="centerobject" style={{
											padding:'0px',
											margin: '1px',
											textAlign: 'center',
											fontFamily: 'Montserrat',
											fontWeight:'600',
											fontSize: '12px',
											color:'#2B678C',
											height:'30px'}}> </h3>
									</SwiperSlide>
								))}
						    </Swiper>


									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center', marginBottom:'100px'}}>
										<Row>
		                  <Col className="ml-auto mr-auto">
                        <br></br>

                        <h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
                          Review Change Request
                        </h2>

                        {titles2.map((row)=> (
  											<Card className="card-timeline"
														style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px',
														color:'#000000',
														boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',}}
														onClick={this.handleClickPopUpDescription.bind(this, row)}>
  												<CardBody style ={{backgroundColor:'#ffffff', margin:'2px', minHeight:'0px'}}>
                            <h4 className="centerobject" style={{
                              padding:'0px',
                              margin: '0px',
                              textAlign: 'center',
                              fontFamily: 'Montserrat',
                              fontWeight:'600',
                              fontSize: '12px',
                              color:'#2B678C'
                            }}>
                              {row}
                            </h4>
  												</CardBody>
  											</Card>
                        ))}

											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

					<Dialog
						open={this.state.description_popup}
						closeOnDocumentClick
						onClose={this.closeModal}
					>
						{(function(){
								return(
									<div>
										<h4 className="centerobject" style={{
											paddingTop:'5px',
											paddingRight:'20px',
											paddingLeft:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'600',
											fontSize: '16px',
											color:'#2B678C',
											textAlign:'center'
										}}>
											{choosed_detail}
										</h4>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
										<Icon className="fa fa-calendar" fontSize="small" style={{
											marginTop:'5px',
											marginRight:'10px',
										}}/>
											Submission date: {idea_date}
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Priority
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{priority}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Entity
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{entity}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Impacted Application
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{impacted_apps}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Directorate
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{dir}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Expected Go-Live Date
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C',
											marginBottom: '25px'
										}}>
											{golive_date_formatted}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Background
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{background_urs}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Objective
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{objective}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Scope
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{scope}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Deliverable
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{deliverable}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Current State
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{current_state}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Desired State
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C',
											marginBottom: '25px'
										}}>
											{desired_state}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Quantitative Benefit
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{quantitative}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Qualitative Benefit
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{qualitative}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Risk and Assumption
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{risk_assumption}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Successful Matrix
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{succes_matrix}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											UAT Scenario
										</p>
										<p className="centerobject" style={{
											marginBottom:'25px',
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C',
											marginBottom: '25px'
										}}>
											{uat_scenario}
										</p>

										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Business Owner
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{business_owner}
										</p>

										{bp_text ? (
											<div>
												<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingTop:'10px',
												paddingRight:'20px',
												textAlign: 'left',
												fontWeight:'400',
												fontFamily: 'Montserrat',
												fontSize: '12px',
												color:'#8F8F8F'
											}}>
												Business Partner
											</p>
											<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingRight:'20px',
												textAlign: 'left',
												fontFamily: 'Montserrat',
												fontWeight:'500',
												fontSize: '12px',
												color:'#2B678C',
												marginBottom: '25px'
											}}>
												{business_partner}
											</p>
											</div>
										) : null}

										{head_text ? (
											<div>
												<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingTop:'10px',
												paddingRight:'20px',
												textAlign: 'left',
												fontWeight:'400',
												fontFamily: 'Montserrat',
												fontSize: '12px',
												color:'#8F8F8F'
											}}>
												IT Head
											</p>
											<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingRight:'20px',
												textAlign: 'left',
												fontFamily: 'Montserrat',
												fontWeight:'500',
												fontSize: '12px',
												color:'#2B678C',
												marginBottom: '25px'
											}}>
												{it_head}
											</p>
											</div>
										) : null}

										{dev_text ? (
											<div>
												<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingTop:'10px',
												paddingRight:'20px',
												textAlign: 'left',
												fontWeight:'400',
												fontFamily: 'Montserrat',
												fontSize: '12px',
												color:'#8F8F8F'
											}}>
												Developer
											</p>
											<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingRight:'20px',
												textAlign: 'left',
												fontFamily: 'Montserrat',
												fontWeight:'500',
												fontSize: '12px',
												color:'#2B678C',
												marginBottom: '25px'
											}}>
												{developer1}
											</p>
											<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingRight:'20px',
												textAlign: 'left',
												fontFamily: 'Montserrat',
												fontWeight:'500',
												fontSize: '12px',
												color:'#2B678C',
												marginBottom: '25px'
											}}>
												{developer2}
											</p>
											<p className="centerobject" style={{
												paddingLeft:'20px',
												paddingRight:'20px',
												textAlign: 'left',
												fontFamily: 'Montserrat',
												fontWeight:'500',
												fontSize: '12px',
												color:'#2B678C',
												marginBottom: '25px'
											}}>
												{developer3}
											</p>
											</div>
										) : null}
										
										<Form style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											fontWeight:'400'
										}}>
							        <FormGroup>
							          <label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
												fontWeight:'600',
												fontSize: '12px', color:'#2B678C'}}>
												Review Status
												</label>
							          <Input id="exampleFormControlSelect1"
												placeholder="Pilih Status..."
												type="select"
												onChange={handleStatusChange}
												style={{
														color:'#2B678C',
														fontFamily: 'Montserrat',
														fontWeight:'700',
														fontSize: '12px'}}>
											<option disabled selected></option>
											<option style={{color:'#28B15F', fontFamily: 'Montserrat', fontWeight:'700', fontSize: '12px'}}>Approve</option>
											<option style={{color:'#FF0000', fontFamily: 'Montserrat', fontWeight:'700', fontSize: '12px'}}>Decline</option>
							          </Input>
							        </FormGroup>
									{bp_field ?
										<FormGroup>
											<label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
													fontWeight:'600',
													fontSize: '12px', color:'#2B678C'}}>
													Choose Business Partner
													</label>
											<Input id="exampleFormControlSelect1"
													placeholder="Pilih Status..."
													type="select"
													onChange={handleBusinessPartnerChange}
													style={{
															color:'#2B678C',
															fontFamily: 'Montserrat',
															fontWeight:'700',
															fontSize: '12px'}}>
												<option disabled selected></option>
												{list_bp.map((row)=>(
													<option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '12px'}} value={row.email}>{row.name}</option>
												))}
											</Input>
										</FormGroup> : null 
									}
									{head_field ?
										<FormGroup>
											<label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
													fontWeight:'600',
													fontSize: '12px', color:'#2B678C'}}>
													Choose IT Head
													</label>
											<Input id="exampleFormControlSelect1"
													placeholder="Pilih Status..."
													type="select"
													onChange={handleItHeadChange}
													style={{
															color:'#2B678C',
															fontFamily: 'Montserrat',
															fontWeight:'700',
															fontSize: '12px'}}>
												<option disabled selected></option>
												{list_head.map((row)=>(
													<option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '12px'}} value={row.email}>{row.name}</option>
												))}
											</Input>
										</FormGroup> : null 
									}
									{dev_field ?
										<div>
											<FormGroup>
												<label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
														fontWeight:'600',
														fontSize: '12px', color:'#2B678C'}}>
														Choose 1st Developer
														</label>
												<Input id="exampleFormControlSelect1"
														placeholder="Pilih Status..."
														type="select"
														onChange={handleDeveloper1Change}
														style={{
																color:'#2B678C',
																fontFamily: 'Montserrat',
																fontWeight:'700',
																fontSize: '12px'}}>
													<option disabled selected></option>
													{list_dev.map((row)=>(
														<option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '12px'}} value={row.email}>{row.name}</option>
													))}
												</Input>
											</FormGroup>
											<FormGroup>
												<label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
														fontWeight:'600',
														fontSize: '12px', color:'#2B678C'}}>
														Choose 2nd Developer
														</label>
												<Input id="exampleFormControlSelect1"
														placeholder="Pilih Status..."
														type="select"
														onChange={handleDeveloper2Change}
														style={{
																color:'#2B678C',
																fontFamily: 'Montserrat',
																fontWeight:'700',
																fontSize: '12px'}}>
													<option disabled selected></option>
													{list_dev.map((row)=>(
														<option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '12px'}} value={row.email}>{row.name}</option>
													))}
												</Input>
											</FormGroup>
											<FormGroup>
												<label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
														fontWeight:'600',
														fontSize: '12px', color:'#2B678C'}}>
														Choose 3rd Developer
														</label>
												<Input id="exampleFormControlSelect1"
														placeholder="Pilih Status..."
														type="select"
														onChange={handleDeveloper3Change}
														style={{
																color:'#2B678C',
																fontFamily: 'Montserrat',
																fontWeight:'700',
																fontSize: '12px'}}>
													<option disabled selected></option>
													{list_dev.map((row)=>(
														<option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '12px'}} value={row.email}>{row.name}</option>
													))}
												</Input>
											</FormGroup>
										</div> : null 
									}
									{comment_field ?
										<FormGroup>
										<label htmlFor="exampleFormControlTextarea1" style={{textAlign: 'left', fontFamily: 'Montserrat',
													fontWeight:'600',
													fontSize: '12px', color:'#2B678C'}}>
										Comment
										</label>
										<Input
														id="exampleInputPassword1"
														placeholder="Komentar..."
														type="textarea"
														style={{ height: 100, fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '12px', color:'#2B678C'}}
														onChange={handleCommentChange}>
													</Input>
										</FormGroup> : null
									}
							      </Form>
									</div>
								)
						})()}

						<div className="centerobject" style={{widht:'100%', textAlign: 'center', marginTop:'10px'}}>
							<Button
								className="centerobject btn-round"
								color="default"
								type="button"
								style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'50%', marginBottom:'10px'}}
								onClick={this.handleSubmit.bind(this, chosen_task)}
							>
								Update
							</Button>
						</div>

					</Dialog>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
