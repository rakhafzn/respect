/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';
import Datetime from "react-datetime";

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import Icon from '@material-ui/core/Icon';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		const cookies = new Cookies()
		super();
		this.state = {
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			isSuperior : false,
		};
	}

  componentWillMount() {

	}

	approveTugas = event => {
		console.log("Redirect to Approval Page")
	}

	componentWillMount(){
		// console.log(this.state.user_email)
		axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_form?status=eq.request')
		.then(response => {
			console.log(response.data.length)
			console.log(response.data)
			this.setState({
				idea_submitted : response.data,
				idea_total : response.data.length
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

	render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log('Review IP')
		console.log(this.state.login_status)
		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			var today = new Date()
			var renderers = {
			  renderDay: function( props, currentDate, selectedDate ){
			    return <td {...props}>{currentDate.date() }</td>;
			  },
			  renderMonth: function( props, month, year, selectedDate){
			    return <td {...props}>{ month }</td>;
			  },
			  renderYear: function( props, year, selectedDate ){
			    return <td {...props}>{ year % 100 }</td>;
			  }
			}
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', padding:'0px', marginBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'130px'}}>
	              <Container>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
										<Row>
		                  <Col className="ml-auto mr-auto">
												<Link to="/review-details">
													<Card
														className="card-register"
														onClick={this.approveTugas.bind(this)}
														style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px', color:'#333333', padding:'10px',
														color:'#333333',
														boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',}}>
		                        <CardBody>
															<h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
																<Icon className="fa fa-info-circle" color="secondary" fontSize="small" style={{
																	paddingTop:'1px',
																	marginRight:'5px',
																}}/>

																<b>{this.state.idea_total} Project</b> untuk direview
																<i className="nc-icon nc-minimal-right" style={{marginLeft:'20px'}}/>

															</h2>
														</CardBody>
		                      </Card>
												</Link>

											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
