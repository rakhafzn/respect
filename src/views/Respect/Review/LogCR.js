/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios';
import Cookies from 'universal-cookie';
import Swal from 'sweetalert2';
import Select from 'react-select';
import { Button, Card, CardBody, CardTitle, CardHeader,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col, UncontrolledCollapse } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { makeStyles, Typography, TextField, FormControl } from '@material-ui/core';
// core comments

// <div className="content-center">

export class SubmitForm extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			name: null,
			directorate: null,
			judulProject: null,
      businessOwner: null,
      priority: null,
      entity: null,
      impactedApps: null,
      goliveDate: null,
      ref: null,
		})
	}

	componentDidMount(){
      axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_form?or=(requester_email.eq.'+this.state.user_email+')')
      .then(response => {
        console.log(response.data)
        this.setState({
          list_urs : response.data
        })
      })
      .catch(error => {
        console.log(error)
        console.log('GAGAL')
      })
	}

    

  render(){
		


		//#2B678C
    if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
        console.log(this.state.userloggedin.display_name)
        console.log(this.state.user_email)
        const display_name = this.state.userloggedin.display_name
        return (
	      <>
	        <div >
	          <div
				style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'65px',marginBottom:'100px'}}>
	                <Container>
                        
                        <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
                            Hello, {this.state.userloggedin.display_name}!
                        </h2>
                        <div className="title-brand">
                        <h1 style={{fontSize: '18px', fontWeight:'400', fontFamily: 'Montserrat', marginTop:'15px', color:'#2B678C'}}>
                            <b>Welcome to Respect!</b>
                        </h1>
                        </div>
                        <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', marginBottom:'10px', fontWeight:'600', fontSize:'16px', color:'#2B678C'}}>
                            Log CR
                        </h2>

                        
                        <Card  style ={{
													backgroundColor:'#ffffff',
													margin:'0px',
													padding:'0px',
													minHeight:'0px',
													width:'100%',
													color:'#333333',
													boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
												}}>
	                        <CardHeader className="card-collapse" id="headingOne" role="tab" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
	                          <h5 className="mb-0 panel-title" style={{margin:'5px', fontWeight:'600', fontSize:'16px'}}>
                              Cek status CR kamu disini
                              <i className="nc-icon nc-minimal-down" />
	                          </h5>
	                        </CardHeader>
	                        {/* <UncontrolledCollapse toggler="#headingOne"> */}
	                          <CardBody style={{paddingTop:'0px'}}>
															<div>
																<Row>
																	<Col xs="1" sm="1" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>ID</b>
																		</h4>
																	</Col>
																	<Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px', textAlign:'left'}}>
																			<b>CR Title</b>
																		</h4>
																	</Col>
                                                                    <Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>Submit Date</b>
																		</h4>
																	</Col>
                                                                    <Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>Approve Date</b>
																		</h4>
																	</Col>
                                                                    <Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>Finish Date</b>
																		</h4>
																	</Col>
																	<Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>Status</b>
																		</h4>
																	</Col>
																	<Col xs="1" sm="1" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px', textAlign:'center'}}>
																			<b>Action</b>
																		</h4>
																	</Col>
																</Row>
															</div>
															
	                          </CardBody>
	                        {/* </UncontrolledCollapse> */}
	                      </Card>
						  
                            
                            
     
                  </Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default SubmitForm;
