/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios';
import Cookies from 'universal-cookie';
import Swal from 'sweetalert2';
import Select from 'react-select';
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col, UncontrolledCollapse } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { makeStyles, Typography, TextField, FormControl } from '@material-ui/core';
// core comments

// <div className="content-center">

export class SubmitForm extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			name: null,
			directorate: null,
			judulProject: null,
      businessOwner: null,
      priority: null,
      entity: null,
      impactedApps: null,
      goliveDate: null,
      ref: null,
		})
	}

	componentDidMount(){
      axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_form?or=(requester_email.eq.'+this.state.user_email+')')
      .then(response => {
        console.log(response.data)
        this.setState({
          list_urs : response.data
        })
      })
      .catch(error => {
        console.log(error)
        console.log('GAGAL')
      })
	}

  

  handleClickCheckUrs = event => {
    console.log(this.state.idUrs)
    axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_form?id=eq.'+this.state.idUrs)
			.then(response => {
        var urs_status = '-'
        if (response.data[0].status === 'draft') {
          urs_status = 'Draft'
        } else if (response.data[0].status === 'request') {
          urs_status = 'Submitted'
        } else if (response.data[0].status === 'approved1') {
          urs_status = 'Approved by Business Owner'
        } else if (response.data[0].status === 'approved2') {
          urs_status = 'Reviewed by IT Business Partner'
        } else if (response.data[0].status === 'approved3') {
          urs_status = 'Assigned to Developer'
        } else if (response.data[0].status === 'assessed') {
          urs_status = 'On Development'
        } else if (response.data[0].status === 'done') {
          urs_status = 'Done'
        } else if (response.data[0].status === 'rejected') {
          urs_status = 'Rejected'
        }
        Swal.fire({
          icon: 'info',
          text: 'The Status of '+ response.data[0].project_name+ ' is ' + urs_status,
          confirmButtonColor: 'blue',
         })
         console.log(response.data[0].id)
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          text: 'ID Change Request not found, please check the project ID on the first column of the tracking page on Respect!',
          confirmButtonColor: 'blue',
         })
      })
    
  }

  handleChangeFindId = event => {
    this.setState({
			idUrs: event.target.value
		})
  }

  render(){
		


    const handleChangeFindId = this.handleChangeFindId.bind(this)
    const handleClickCheckUrs = this.handleClickCheckUrs.bind(this)

    const options = [
        { value: 'low', label: 'Low' },
        { value: 'medium', label: 'Medium' },
        { value: 'high', label: 'High' }
    ]

		//#2B678C
    if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
        console.log(this.state.userloggedin.display_name)
        console.log(this.state.user_email)
        const display_name = this.state.userloggedin.display_name
        return (
	      <>
	        <div >
	          <div
				style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'65px',marginBottom:'100px'}}>
	                <Container>
                        
                        <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
                            Hello, {this.state.userloggedin.display_name}!
                        </h2>
                        <div className="title-brand">
                        <h1 style={{fontSize: '18px', fontWeight:'400', fontFamily: 'Montserrat', marginTop:'15px', color:'#2B678C'}}>
                            <b>Welcome to Respect!</b>
                        </h1>
                        </div>
                        <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', marginBottom:'10px', fontWeight:'600', fontSize:'16px', color:'#2B678C'}}>
                            Track your Change Request status here!
                        </h2>

                        
                            <div className="title-brand">
                              {/* <Button id="buttonToggler2" style={{backgroundColor:"rgba(50,50,50,50)", margin:'10px', borderRadius:'20px', borderColor: '#ffffff'}}>
                                Track Your Idea
                              </Button>
                              <UncontrolledCollapse toggler="#linkToggler,#buttonToggler2"> */}
                                <div style={{paddingLeft:'10px', paddingRight:'10px'}}>
                                  <Card style={{backgroundColor:'rgba(255,255,255,1)', padding:'10px', width:'100%'}}>
                                    <TextField style={{
                                      margin: '5px',
                                      width: '95%',
                                    }}
                                      type="text"
                                      variant="outlined"
                                      margin="normal"
                                      required
                                      fullWidth
                                      label="CR ID"
                                      id="standard-multiline-flexible"
                                      onChange={handleChangeFindId}
                                    >
                                    </TextField>
                                    <div className="centerobject" style={{widht:'100%'}}>
                                      <Button
                                        className="btn-round"
                                        color="default"
                                        type="button"
                                        id="buttonToggler3"
                                        onClick={handleClickCheckUrs}
                                        style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'50%'}}
                                      >
                                        Track
                                      </Button>
                                    </div>
                                  
                                  </Card>
                                  
                                  </div>
                              {/* </UncontrolledCollapse> */}
                              
                            </div>
                            
                            
     
                  </Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default SubmitForm;
