/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';
import * as emailjs from 'emailjs-com'

// reactstrap components
import axios from 'axios';
import Cookies from 'universal-cookie';
import Swal from 'sweetalert2';
import Select from 'react-select';
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { makeStyles, Typography, TextField, FormControl } from '@material-ui/core';
import CardHeader from "reactstrap/lib/CardHeader";
import UncontrolledCollapse from "reactstrap/lib/UncontrolledCollapse";
// core comments

// <div className="content-center">

export class SubmitForm extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
      user_role: cookies.get('user_role'),
			name: null,
			directorate: "-",
			judulProject: "-",
      businessOwner: "-",
      priority: "-",
      entity: "-",
      impactedApps: "-",
      goliveDate: "-",
      ref: "-",
      objective: "-",
      background: "-",
      scope: "-",
      deliverable: "-",
      current_state: "-",
      desired_state: "-",
      quantitative_benefit: "-",
      qualitative_benefit: "-",
      risk_assumption: "-",
      success_matrix: "-",
      uat_scenario: "-",
      business_owner: [],
		})
	}

	componentDidMount(){
    axios.get('https://sfa-api-testing.pti-cosmetics.com/respect_user?role=eq.BUSINESS%20OWNER')
		.then(response => {
			console.log(response.data.length)
			console.log(response.data)
			this.setState({
				business_owner : response.data,
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

    handleChangeDirectorate = event => {
		// console.log(event.target.value)
		this.setState({
			directorate: event.target.value
		})
	}
	handleChangeJudulProject = event => {
		// console.log(event.target.value)
		this.setState({
			judulProject: event.target.value
		})
	}
    handleChangeBusinessOwner = event => {
		// console.log(event.target.value)
		this.setState({
			businessOwner: event.target.value
		})
	}
    handleChangePriority = event => {
		// console.log(event.target.value)
		this.setState({
			priority: event.target.value
		})
	}
    handleChangeEntity = event => {
		// console.log(event.target.value)
		this.setState({
			entity: event.target.value
		})
	}
    handleChangeImpactedApps = event => {
		// console.log(event.target.value)
		this.setState({
			impactedApps: event.target.value
		})
	}
    handleChangeGoLiveDate = event => {
		// console.log(event.target.value)
		this.setState({
			goliveDate: event.target.value
		})
	}
  handleChangeObjective = event => {
		// console.log(event.target.value)
		this.setState({
			objective: event.target.value
		})
	}
	handleChangeBackground = event => {
		// console.log(event.target.value)
		this.setState({
			background: event.target.value
		})
	}
    handleChangeScope = event => {
		// console.log(event.target.value)
		this.setState({
			scope: event.target.value
		})
	}
    handleChangeDeliverable = event => {
		// console.log(event.target.value)
		this.setState({
			deliverable: event.target.value
		})
	}
    handleChangeCurrentState = event => {
		// console.log(event.target.value)
		this.setState({
			current_state: event.target.value
		})
	}
    handleChangeDesiredState = event => {
		// console.log(event.target.value)
		this.setState({
			desired_state: event.target.value
		})
	}
  handleChangeQuantitativeBenefit = event => {
		// console.log(event.target.value)
		this.setState({
			quantitative_benefit: event.target.value
		})
	}
	handleChangeQualitativeBenefit = event => {
		// console.log(event.target.value)
		this.setState({
			qualitative_benefit: event.target.value
		})
	}
    handleChangeRiskAssumption = event => {
		// console.log(event.target.value)
		this.setState({
			risk_assumption: event.target.value
		})
	}
    handleChangeSuccessfulMatrix = event => {
		// console.log(event.target.value)
		this.setState({
			success_matrix: event.target.value
		})
	}
    handleChangeUatScenario = event => {
		// console.log(event.target.value)
		this.setState({
			uat_scenario: event.target.value
		})
	}

	handleFileInput = event => {
    console.log('this is input file')
    this.setState({[event.target.name]: event.target.files[0]})
    console.log(event.target.name)
    console.log(event.target.files[0])
  }

	handleClickUploadFile = event => {
    if (this.state.ktp_pdf != null) {
      console.log('Upload file siap disimpan')
      var fd = new FormData();
      fd.append('ktp_pdf', this.state.ktp_pdf);

      const options={
        onUploadProgress: (progressEvent) => {
          const{loaded,total} = progressEvent;
          let percent = Math.floor((loaded*100)/total)
          console.log(`${loaded}kb of ${total}kb | ${percent}%`);
          this.setState({
            progressStatus : `${loaded} kb of ${total} kb | ${percent}%`
          })
          if(percent < 100){
            this.setState({ uploadPercentage: percent})
          }
        }
      }
			// simpan di 10.3.181.123
      axios.post('https://testing-bivi-007.pti-cosmetics.com/bivi-api/index.php/api/docprofileip', fd, options)
      .then(response => {
        console.log(response.data)
        this.setState({uploadPercentage:100}, ()=>{
          setTimeout(()=>{
            this.setState({uploadPercentage:0})
          },1000)
        })
        var status = response.data.status
        console.log(response.status)
        if(status === true){
          axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?order=id.desc&limit=1')
          .then(response => {
            console.log(response.data)
						console.log(this.state.ktp_pdf.name)
            this.setState({
              file_uploaded : response.data[0],
				      is_file_uploaded: true,
            })
            Swal.fire({
             icon: 'success',
             text: 'Your file has been uploaded',
             confirmButtonColor: 'blue',
            });
          })
          .catch(error => {
            console.log(error)
          })
        }
        // this.props.history.goBack();
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          text: 'Form cant be submitted!',
          confirmButtonColor: '#e55555',
        });
        // this.props.history.goBack();
        console.log(error.response)
      })
    } else {
      Swal.fire({
        icon: 'error',
        text: 'Upload File Failed!',
        confirmButtonColor: '#e55555',
      });
    }
  }

	handleSubmit = event =>{
		if(this.state.directorate === null || this.state.judulProject === null || this.state.businessOwner === null 
            || this.state.priority === null || this.state.entity === null || this.state.impactedApps === null
            || this.state.goliveDate === null || this.state.background === null || this.state.objective === null
            || this.state.scope === null || this.state.deliverable === null || this.state.current_state === null
            || this.state.desired_state === null || this.state.quantitative_benefit === null || this.state.qualitative_benefit === null
            || this.state.risk_assumption === null || this.state.success_matrix === null || this.state.uat_scenario === null
            || this.state.directorate === "-" || this.state.judulProject === "-" || this.state.businessOwner === "-" 
            || this.state.priority === "-" || this.state.entity === "-" || this.state.impactedApps === "-"
            || this.state.goliveDate === "-" || this.state.background === "-" || this.state.objective === "-"
            || this.state.scope === "-" || this.state.deliverable === "-" || this.state.current_state === "-"
            || this.state.desired_state === "-" || this.state.quantitative_benefit === "-" || this.state.qualitative_benefit === "-"
            || this.state.risk_assumption === "-" || this.state.success_matrix === "-" || this.state.uat_scenario === "-") {
			Swal.fire({
          icon: 'error',
          text: 'Complete your form!',
          confirmButtonColor: '#e55555',
      });
		} else {
      const reference = this.state.userloggedin.display_name.charAt(0)+this.state.businessOwner.charAt(0)
          +this.state.priority.charAt(0)+this.state.judulProject.charAt(0)+this.state.directorate.charAt(0)
          +this.state.goliveDate.charAt(8)+this.state.goliveDate.charAt(9)+this.state.goliveDate.charAt(5)
          +this.state.goliveDate.charAt(6)
      console.log(reference)
			var urs_data = {
				"requester_name": this.state.userloggedin.display_name,
				"requester_email": this.state.user_email,
				"directorate": this.state.directorate,
				"project_name": this.state.judulProject,
				"business_owner": this.state.businessOwner,
				"priority": this.state.priority,
				"entity": this.state.entity,
				"impacted_apps": this.state.impactedApps,
        "golive_date": this.state.goliveDate,
				"status": "request",
        "ref": reference,
        "background": this.state.background,
        "objective": this.state.background,
        "scope": this.state.scope,
        "deliverable": this.state.deliverable,
        "current_state": this.state.current_state,
        "desired_state": this.state.desired_state,
        "quantitative_benefit": this.state.quantitative_benefit,
        "qualitative_benefit": this.state.qualitative_benefit,
        "risk_assumption": this.state.risk_assumption,
        "success_matrix": this.state.success_matrix,
        "uat_scenario": this.state.uat_scenario,
			}
      var log_data ={
        "reference": reference,
        "action": "submit",
        "email": this.state.user_email,
        "urs": this.state.judulProject,
      }
      console.log(reference)
			console.log(urs_data)
			axios.post('https://sfa-api-testing.pti-cosmetics.com/urs_form', urs_data)
			.then(response => {
				console.log('URS berhasil di submit')
        axios.post('https://sfa-api-testing.pti-cosmetics.com/urs_log', log_data)
        .then(response2=>{
          emailjs.send("service_615ys9m","template_nqncl9k",{
            from_name: this.state.userloggedin.display_name,
            message: this.state.userloggedin.display_name + " submitted new URS: " + this.state.judulProject + ". Please check it on Respect!",
            to_email: this.state.businessOwner,
            }, "user_h1mWZcdzQiHbtquLsKZ5y"
            );
          Swal.fire({
            icon: 'success',
            text: 'Change Request is submitted',
            confirmButtonColor: 'blue',
          }).then(function (result) {
            if (true) {
              window.location = "/check";
            }
          });
        })	
      })
      .catch(error => {
        console.log(error)
        console.log('GAGAL')
      })
				// Swal.fire({
        //  icon: 'success',
        //  text: 'Your Proposal has been submitted',
        //  confirmButtonColor: 'blue',
        // });
        // window.location.reload();
		}
	}

  handleDraft = event =>{
		if(this.state.directorate === null || this.state.judulProject === null || this.state.businessOwner === null 
            || this.state.priority === null || this.state.entity === null || this.state.impactedApps === null
            || this.state.goliveDate === null) {
			Swal.fire({
          icon: 'error',
          text: 'Complete your form!',
          confirmButtonColor: '#e55555',
      });
		} else {
      const reference = this.state.userloggedin.display_name.charAt(0)+this.state.businessOwner.charAt(0)
          +this.state.priority.charAt(0)+this.state.judulProject.charAt(0)+this.state.directorate.charAt(0)
          +this.state.goliveDate.charAt(8)+this.state.goliveDate.charAt(9)+this.state.goliveDate.charAt(5)
          +this.state.goliveDate.charAt(6)
      console.log(reference)
			var urs_data = {
				"requester_name": this.state.userloggedin.display_name,
				"requester_email": this.state.user_email,
				"directorate": this.state.directorate,
				"project_name": this.state.judulProject,
				"business_owner": this.state.businessOwner,
				"priority": this.state.priority,
				"entity": this.state.entity,
				"impacted_apps": this.state.impactedApps,
        "golive_date": this.state.goliveDate,
				"status": "draft",
        "ref": reference,
        "background": this.state.background,
        "objective": this.state.objective,
        "scope": this.state.scope,
        "deliverable": this.state.deliverable,
        "current_state": this.state.current_state,
        "desired_state": this.state.desired_state,
        "quantitative_benefit": this.state.quantitative_benefit,
        "qualitative_benefit": this.state.qualitative_benefit,
        "risk_assumption": this.state.risk_assumption,
        "success_matrix": this.state.success_matrix,
        "uat_scenario": this.state.uat_scenario,
			}
      console.log(reference)
			console.log(urs_data)
			axios.post('https://sfa-api-testing.pti-cosmetics.com/urs_form', urs_data)
			.then(response => {
				console.log('URS saved as draft!')
				Swal.fire({
                    icon: 'success',
                    text: 'Change Request saved as draft',
                    confirmButtonColor: 'blue',
                }).then(function (result) {
					  if (true) {
					    window.location = "/check";
					  }
					});
				})
				.catch(error => {
					console.log(error)
					console.log('GAGAL')
				})
				// Swal.fire({
        //  icon: 'success',
        //  text: 'Your Proposal has been submitted',
        //  confirmButtonColor: 'blue',
        // });
        // window.location.reload();
		}
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.login_status)
    console.log(this.state.user_role)
		console.log(this.state.businessOwner)
		console.log(this.state.directorate)
		console.log(this.state.judulProject)
		console.log(this.state.priority)
		console.log(this.state.entity)
		console.log(this.state.impactedApps)
		console.log(this.state.goliveDate)
    console.log(this.state.ref)
    console.log(this.state.background)
		console.log(this.state.objective)
		console.log(this.state.scope)
		console.log(this.state.deliverable)
		console.log(this.state.current_state)
		console.log(this.state.desired_state)
    console.log(this.state.business_owner)
		// console.log(this.state.file_uploaded)
		// console.log(this.state.ktp_pdf)
		// console.log(this.state.ktp_pdf)


		const file_uploaded = this.state.file_uploaded
    const is_file_uploaded = this.state.is_file_uploaded
    // const handleChangeName = this.handleChangeName.bind(this)
    const handleChangeDirectorate = this.handleChangeDirectorate.bind(this)
    const handleChangeJudulProject = this.handleChangeJudulProject.bind(this)
    const handleChangeBusinessOwner = this.handleChangeBusinessOwner.bind(this)
    const handleChangePriority = this.handleChangePriority.bind(this)
    const handleChangeEntity = this.handleChangeEntity.bind(this)
    const handleChangeImpactedApps = this.handleChangeImpactedApps.bind(this)
    const handleChangeGoLiveDate = this.handleChangeGoLiveDate.bind(this)
    const handleChangeObjective = this.handleChangeObjective.bind(this)
    const handleChangeBackground = this.handleChangeBackground.bind(this)
    const handleChangeScope = this.handleChangeScope.bind(this)
    const handleChangeDeliverable = this.handleChangeDeliverable.bind(this)
    const handleChangeCurrentState = this.handleChangeCurrentState.bind(this)
    const handleChangeDesiredState = this.handleChangeDesiredState.bind(this)
    const handleChangeQuantitativeBenefit = this.handleChangeQuantitativeBenefit.bind(this)
    const handleChangeQualitativeBenefit = this.handleChangeQualitativeBenefit.bind(this)
    const handleChangeRiskAssumption = this.handleChangeRiskAssumption.bind(this)
    const handleChangeSuccessfulMatrix = this.handleChangeSuccessfulMatrix.bind(this)
    const handleChangeUatScenario = this.handleChangeUatScenario.bind(this)
    const handleSubmit = this.handleSubmit.bind(this)
    const handleDraft = this.handleDraft.bind(this)

    const options = [
        { value: 'low', label: 'Low' },
        { value: 'medium', label: 'Medium' },
        { value: 'high', label: 'High' }
    ]

		//#2B678C
    if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
        console.log(this.state.userloggedin.display_name)
        console.log(this.state.user_email)
        const display_name = this.state.userloggedin.display_name
        return (
	      <>
	        <div >
	          <div
				style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'65px',marginBottom:'100px'}}>
	                <Container>
                        <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
                            Hello, {this.state.userloggedin.display_name}!
                        </h2>
                        <div className="title-brand">
                        <h1 style={{fontSize: '18px', fontWeight:'400', fontFamily: 'Montserrat', marginTop:'15px', color:'#2B678C', marginBottom:'10px'}}>
                            <b>Welcome to Respect!</b>
                        </h1>
                        </div>
                        <Row>
                                <Col className="ml-auto mr-auto">
                                    <Card style ={{
                                        backgroundColor:'#ffffff',
                                        padding:'30px',
                                        margin:'15px',
                                        minHeight:'0px',
                                        color:'#333333',
                                        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
                                    }}>
                                        <CardHeader className="card-collapse" id="firstpart" role="tab" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
                                          <h5 className="mb-0 panel-title" style={{margin:'5px', fontWeight:'600', fontSize:'16px',color:'#2B678C'}}>
                                            Submit your Change Request here!
                                            <i className="nc-icon nc-minimal-down" />
                                          </h5>
                                        </CardHeader>
                                        <UncontrolledCollapse toggler="firstpart">

                                          <CardBody style={{padding:'5px', margin:'10px'}}>

                                              <Form className="homeGrid">
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Project Name
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Name of the Project"
                                                          type="name"
                                                          onChange={handleChangeJudulProject}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Business Owner
                                                      </Label>
                                                      <Input
                                                          aria-describedby="emailHelp"
                                                          id="exampleInputEmail1"
                                                          placeholder="Email of Business Owner"
                                                          type="select"
                                                          onChange={handleChangeBusinessOwner}
                                                      >
                                                        <option disabled selected style={{color:'#555555', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Business Owner</option>
                                                        {this.state.business_owner.map((row)=>(
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}} value={row.email}>{row.name}</option>
                                                        ))}
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Directorate
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Directorate"
                                                          type="name"
                                                          onChange={handleChangeDirectorate}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Priority
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Priority"
                                                          type="select"
                                                          onChange={handleChangePriority}
                                                      >
                                                          <option disabled selected style={{color:'#555555', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Priority</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Low</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Medium</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>High</option>
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Entity
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Entity"
                                                          type="select"
                                                          onChange={handleChangeEntity}
                                                      >
                                                          <option disabled selected style={{color:'#555555', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Entity</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Paragon</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Parama</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Varcos</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Paraversa</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>Pharmacore</option>
                                                          <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'600', fontSize: '16px'}}>All</option>
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Impacted Application
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Impacted Application"
                                                          type="name"
                                                          onChange={handleChangeImpactedApps}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'45px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Expected Go Live
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Expected Go Live"
                                                          type="date"
                                                          onChange={handleChangeGoLiveDate}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Background
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Provide background information that includes the reasons for creating the project and mentions the key
                                                          stakeholder who will benefit from the project result"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeBackground}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Objective
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Describe the project goals and link each of them with related, SMART project objectives"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeObjective}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Project Scope
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Provide a high-level description of the features and functions that characterize the product, service, or result
                                                          the project is meant to deliver"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeScope}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Project Deliverable
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Define the key deliverables that the project is required to produce in order to achieve the stated objectives."
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeDeliverable}
                                                      >
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                  <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Current State
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Describe current process that you expect to be changed"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeCurrentState}
                                                      >
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'45px'}}>
                                                  <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Desired State
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Describe the new process that you expect. Focus on the changes!"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeDesiredState}
                                                      >
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Quantitative Benefit
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="The measurable improvement (e.g.: increasing revenues XX by XX, cost saving XX by XX, productivity
                                                            gain/process improvement by reducing manual works XX by XX, cost avoidance XX in XX)"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeQuantitativeBenefit}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Qualitative Benefit
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Including quality management, improved project requirements compliance and regulation, secured access to
                                                          corporate information, increased staff motivation, etc."
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeQualitativeBenefit}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Risk and Assumptions
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Risk: Outline the risks identified at the start of the project. Include a quick assessment of the significance of
                                                          each risk and how to address them.
                                                          Assumption: Specify all factors that are, for planning purposes considered to be true. During the planning
                                                          process, these assumptions will be validated."
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeRiskAssumption}
                                                      ></Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                      <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          Successful Matrix
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="A measurement of success, either against peers or against a predetermined target (e.g.: the break-even
                                                            point, leads generated and leads converted, sales indicators, net income ratio/profit, customers (new, repeat
                                                            and referrals), employee satisfaction."
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeSuccessfulMatrix}
                                                      >
                                                      </Input>
                                                  </FormGroup>
                                                  <FormGroup style={{marginBottom:'25px'}}>
                                                  <Label style={{color:'#2B678C', fontSize:'16px'}}>
                                                          UAT Scenario
                                                      </Label>
                                                      <Input
                                                          id="exampleInputPassword1"
                                                          placeholder="Describe the scenario of acceptance test"
                                                          type="textarea"
                                                          style={{ 
                                                            height: 100, 
                                                            fontFamily: 'Montserrat',
                                                            fontWeight:'400',
                                                            fontSize: '16px', 
                                                            color:'#2B678C'
                                                          }}
                                                          onChange={handleChangeUatScenario}
                                                      >
                                                      </Input>
                                                  </FormGroup>
                                                  <div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
                                                      <Button
                                                          className="btn-round"
                                                          color="default"
                                                          type="button"
                                                          style={{
                                                              backgroundColor:'#2B678C', 
                                                              borderColor:'#2B678C',
                                                              width:'50%', 
                                                              marginTop:'10px'}}
                                                          onClick={handleDraft}
                                                      >
                                                          Save as Draft
                                                      </Button>
                                                      <Button
                                                          className="btn-round"
                                                          color="default"
                                                          type="button"
                                                          style={{
                                                              backgroundColor:'#F1A124', 
                                                              borderColor:'#F1A124',
                                                              width:'50%', 
                                                              marginTop:'10px'}}
                                                          onClick={handleSubmit}
                                                      >
                                                          Submit
                                                      </Button>
                                                  </div>
                                              </Form>
                                          </CardBody>
                                        </UncontrolledCollapse>
                                    </Card>
                                </Col>
                            </Row>
                    </Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default SubmitForm;
