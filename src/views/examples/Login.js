import React from "react";

import axios from 'axios'
import Cookies from 'universal-cookie'
// reactstrap components
import {
  Button,
  Card,
  CardTitle,
  Form,
  Input,
  Container,
  Row,
  Col
} from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

// core components
import ColorNavbar from "components/Navbars/ColorNavbar.js";
import ColorNavbarIP from "components/Navbars/ColorNavbarIP.js";
import ColorNavbarIPLogin from "components/Navbars/ColorNavbarIPLogin.js";
import ColorNavbarIdeation from "components/Navbars/ColorNavbarIdeation.js";

import DownloadPanduanIP from "views/IP/Submission/DownloadPanduanIP.js";
import FooterBlackIdeation from "components/Footers/FooterBlackIdeation.js";

import readXlsxFile from 'read-excel-file'

export class LoginPage extends React.Component {
	constructor() {
    const cookies = new Cookies()
		super();
		this.state = {
      userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			color: [],
      user_email: null,
      user_pass: null,
      encrypt_email: null,
      encrypt_pass: null,
      checkbox: false,
      messageSignin: null
		};
	}

  componentDidMount() {

	}

  encrypt = (event,obj) => {
    var encrypted = ""
    var keyLength = 100

    for (var i = 0; i < event.length; i++) {
      var ch = event.charAt(i)
      var ascii = event.charCodeAt(i)
      if(ascii >= 65 && ascii <= 90){
        var ascii = ascii + (keyLength%26)
        if(ascii > 90) {ascii = ascii - 26}
      } else if(ascii >= 97 && ascii <= 122){
        var ascii = ascii + (keyLength%26)
        if(ascii > 122) {ascii = ascii -26}
      }
      var res = String.fromCharCode(ascii);
      encrypted = encrypted+res;
    }
    return encrypted
  }

  handleChangeInputEmail = event =>{
    console.log(event.target.value)
    this.setState({
			user_email: event.target.value,
      encrypt_email: this.encrypt(event.target.value),
		})
  }

  handleChangeInputPassword = event =>{
    // console.log(event.target.value)
    this.setState({
			user_pass: event.target.value,
      encrypt_pass: this.encrypt(event.target.value),
		})
  }

  handleChangeCheckbox = event =>{
    this.setState({
			checkbox: !this.state.checkbox,
    })
  }

  handleClickLoginButton = event => {
    const qs = require('querystring')
    var credential = this.state.encrypt_email+""+this.state.encrypt_pass

    const requestBody = {
      authentication: credential
    }

    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    axios.post('https://booking-room.pti-cosmetics.com:9666/login', qs.stringify(requestBody), config)
    .then(response => {
      console.log(response.data)
      console.log('DONE : GET RESPONSE FROM LDAP')
      const cookies = new Cookies();
      cookies.set('userloggedin', response.data, {path: '/'});
      cookies.set('login_status', response.data.status, {path: '/'});
      cookies.set('user_email', this.state.user_email, {path: '/'});
      this.setState({
        userloggedin: response.data,
        login_status: response.data.status
      })
      axios.get('https://sfa-api-testing.pti-cosmetics.com/respect_user?email=eq.'+this.state.user_email)
      .then(response2 => {
        console.log(response2.data)
        cookies.set('user_role', response2.data[0].role, {path: '/'});
      })
      .catch(error => {
        console.log(error)
        console.log('error',error.response)
        console.log('Inputed account is not reviewer')
      });
    })
    .catch(error => {
      console.log(error)
      console.log('error',error.response)
      console.log('ERROR : NOT GET RESPONSE')
      this.setState({
        messageSignin: 'Mohon masukkan email dan password dengan benar'
      })
    });

    console.log('finish to auth')
  }

  render(){
    console.log('LOGIN HERE')
    console.log(this.state.userloggedin)
    console.log(this.state.login_status)
    const messageSignin = this.state.messageSignin;
    // console.log(this.state.userloggedin.status)
    if (this.state.login_status === 'true' || this.state.login_status === true) {
      return(
        <Redirect from="*" to="/check" />
      )
    } else {
      return (
        <>
          
          <div className="wrapper" >
            <div
              className="page-header"
              style={{backgroundColor:'#ffffff'}}
            >
              <img style={{width:'35%', zIndex:'0', position:'fixed', top:'20%', left:'10%'}}
                  alt="..."
                  src={require("assets/img/login-image.png")}
                />
            <div style={{marginTop:'120px'}}/>

              <Container style={{marginLeft:'30%'}}>
                <Row>
                  <div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center', width:'100%'}}>
                    <h4 className="centerobject" style={{
                      padding:'5px',
                      textAlign: 'center',
                      margin: '5px',
                      fontFamily: 'Montserrat',
                      fontWeight:'300',
                      fontSize: '18 px',
                      color: '#2B678C',
                      width: '80%'
                    }}>
                      Hello, welcome!
                    </h4>
                    <h3 className="centerobject" style={{
                      padding:'5px',
                      textAlign: 'center',
                      margin: '5px',
                      fontFamily: 'Montserrat',
                      fontWeight:'600',
                      fontSize: '32px',
                      color: '#2B678C',
                      width: '80%'
                    }}>
                      Respect!
                    </h3>
                    <h4 className="centerobject" style={{
                      padding:'5px',
                      textAlign: 'center',
                      marginTop: '10px',
                      marginBottom: '0px',
                      fontFamily: 'Montserrat',
                      fontWeight:'500',
                      fontSize: '18px',
                      color: '#2B678C',
                      width: '80%'
                    }}>
                      Please login with your Paragon account
                    </h4>
                    <h4 className="centerobject" style={{
                      padding:'5px',
                      textAlign: 'center',
                      marginTop: '0px',
                      marginBottom: '0px',
                      fontFamily: 'Montserrat',
                      fontWeight:'500',
                      fontSize: '18px',
                      color: '#2B678C',
                      width: '80%'
                    }}>
                      to submit Change Request
                    </h4>
                  </div>
                  <div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center', width:'100%'}}>
                    <Col className="ml-auto mr-auto" lg="4" md="6" sm="6">
                      <Card style={{
                        backgroundColor:'#ffffff',
                        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
                        color:'black',
                        padding:'30px',
                        margin:'10px'
                      }}>
                        <Form >
                          <Input
                            className="border"
                            placeholder="Email"
                            type="email"
                            style={{marginBottom:'15px'}}
                            onChange={this.handleChangeInputEmail.bind(this)}
                          />
                          <Input
                            className="border"
                            placeholder="Password"
                            type="password"
                            onChange={this.handleChangeInputPassword.bind(this)}
                          />

                          <FormControlLabel
                            style={{justifyContent:'left', display: 'flex', textAlign:'center', width:'100%'}}
                            control={
                              <Checkbox
                                checked={this.state.checkbox}
                                onChange={this.handleChangeCheckbox.bind(this)}
                                name="checkedB"
                                color="primary"
                              />
                            }
                            label=
                              <h4 className="centerobject" style={{
                                padding:'5px',
                                textAlign: 'center',
                                marginTop: '0px',
                                marginBottom: '0px',
                                fontFamily: 'Montserrat',
                                fontWeight:'400',
                                fontSize: '14px',
                                color: 'grey'
                              }}>
                                Remember me
                              </h4>
                          />

                          {(function(){
                            if(messageSignin !== null){
                              return(
                                <p style={{
                                  textAlign: 'center',
                                  fontWeight:'300',
                                  fontSize: '12px',
                                  color: '#e50000',
                                  marginTop:'5px'
                                }}>
                                  {messageSignin}
                                </p>
                              )
                            }
                          })()}

                          <Button
                            className="btn-round"
                            style={{
                              width:'50%',
                              marginTop:'15px',
                              backgroundColor: "#F5B94F",
                              borderColor: "#F5B94F",
                              borderRadius: "20px"
                            }}
                            onClick={this.handleClickLoginButton.bind(this)}
                          >
                            Login
                          </Button>
                        </Form>
                      </Card>
                    </Col>
                  </div>
                </Row>
              </Container>

            </div>
          </div>
        </>
      );
    }
  }
}

export default LoginPage;
