/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import Swal from 'sweetalert2';
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { makeStyles, Typography, TextField, FormControl } from '@material-ui/core';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			name: null,
			directorate: null,
			judulProject: null,
			deskripsiProject: null,
			nama_anggota1: null,
			email_anggota1: null,
			nama_anggota2: null,
			email_anggota2: null,
			nama_superior: null,
			email_superior: null,
      ktp_pdf : null,
      progressStatus: null,
      file_uploaded: {},
      is_file_uploaded: false,
		})
	}

	componentDidMount(){

	}

	// handleChangeName = event => {
	// 	// console.log(event.target.value)
	// 	this.setState({
	// 		name: event.target.value
	// 	})
	// }
	handleChangeDirectorate = event => {
		// console.log(event.target.value)
		this.setState({
			directorate: event.target.value
		})
	}
	handleChangeJudulProject = event => {
		// console.log(event.target.value)
		this.setState({
			judulProject: event.target.value
		})
	}
	handleChangeDeskripsiProject = event => {
		// console.log(event.target.value)
		this.setState({
			deskripsiProject: event.target.value
		})
	}
	handleChangeNamaAnggota1 = event => {
		// console.log(event.target.value)
		this.setState({
			nama_anggota1: event.target.value
		})
	}
	handleChangeEmailAnggota1 = event => {
		// console.log(event.target.value)
		this.setState({
			email_anggota1: event.target.value
		})
	}
	handleChangeNamaAnggota2 = event => {
		// console.log(event.target.value)
		this.setState({
			nama_anggota2: event.target.value
		})
	}
	handleChangeEmailAnggota2 = event => {
		// console.log(event.target.value)
		this.setState({
			email_anggota2: event.target.value
		})
	}
	handleChangeNamaSuperior = event => {
		// console.log(event.target.value)
		this.setState({
			nama_superior: event.target.value
		})
	}
	handleChangeEmailSuperior = event => {
		// console.log(event.target.value)
		this.setState({
			email_superior: event.target.value
		})
	}

	handleFileInput = event => {
    console.log('this is input file')
    this.setState({[event.target.name]: event.target.files[0]})
    console.log(event.target.name)
    console.log(event.target.files[0])
  }

	handleClickUploadFile = event => {
    if (this.state.ktp_pdf != null) {
      console.log('Upload file siap disimpan')
      var fd = new FormData();
      fd.append('ktp_pdf', this.state.ktp_pdf);

      const options={
        onUploadProgress: (progressEvent) => {
          const{loaded,total} = progressEvent;
          let percent = Math.floor((loaded*100)/total)
          console.log(`${loaded}kb of ${total}kb | ${percent}%`);
          this.setState({
            progressStatus : `${loaded} kb of ${total} kb | ${percent}%`
          })
          if(percent < 100){
            this.setState({ uploadPercentage: percent})
          }
        }
      }
			// simpan di 10.3.181.123
      axios.post('https://testing-bivi-007.pti-cosmetics.com/bivi-api/index.php/api/docprofileip', fd, options)
      .then(response => {
        console.log(response.data)
        this.setState({uploadPercentage:100}, ()=>{
          setTimeout(()=>{
            this.setState({uploadPercentage:0})
          },1000)
        })
        var status = response.data.status
        console.log(response.status)
        if(status === true){
          axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?order=id.desc&limit=1')
          .then(response => {
            console.log(response.data)
						console.log(this.state.ktp_pdf.name)
            this.setState({
              file_uploaded : response.data[0],
				      is_file_uploaded: true,
            })
            Swal.fire({
             icon: 'success',
             text: 'Your file has been uploaded',
             confirmButtonColor: 'blue',
            });
          })
          .catch(error => {
            console.log(error)
          })
        }
        // this.props.history.goBack();
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          text: 'Form cant be submitted!',
          confirmButtonColor: '#e55555',
        });
        // this.props.history.goBack();
        console.log(error.response)
      })
    } else {
      Swal.fire({
        icon: 'error',
        text: 'Upload File Failed!',
        confirmButtonColor: '#e55555',
      });
    }
  }

	handleSubmit = event =>{
		if(this.state.directorate === null || this.state.judulProject === null ||
		this.state.nama_superior == null || this.state.email_superior == null) {
			Swal.fire({
        icon: 'error',
        text: 'Complete your form!',
        confirmButtonColor: '#e55555',
      });
		} else {
			var idea_data = {
				"name": this.state.userloggedin.display_name,
				"email": this.state.user_email,
				"directorate": this.state.directorate,
				"idea_title": this.state.judulProject,
				"idea_desc": this.state.deskripsiProject,
				"nama_anggota1": this.state.nama_anggota1,
				"email_anggota1": this.state.email_anggota1,
				"nama_anggota2": this.state.nama_anggota2,
				"email_anggota2": this.state.email_anggota2,
				"nama_superior": this.state.nama_superior,
				"email_superior": this.state.email_superior,
				"status": "review",
			}
			var log_data = {
				"user": this.state.userloggedin.display_name,
				"email": this.state.user_email,
				"idea_title": this.state.judulProject,
				"task": "submit",
				"email_superior": this.state.email_superior,
				"filename": this.state.file_uploaded.filename,
			}
			console.log(idea_data)
			console.log(log_data)
			axios.patch('https://training-api.pti-cosmetics.com/idea_submited_ip?id=eq.'+this.state.file_uploaded.id, idea_data)
			.then(response => {
				console.log('Idea berhasil di submit')
				axios.post('https://training-api.pti-cosmetics.com/idea_task_log', log_data)
				.then(response => {
					console.log('Log berhasil dibuat')
					Swal.fire({
	         icon: 'success',
	         text: 'Your Proposal has been submitted',
	         confirmButtonColor: 'blue',
	        }).then(function (result) {
					  if (true) {
					    window.location = "/ip";
					  }
					});
				})
				.catch(error => {
					console.log(error)
					console.log('GAGAL')
				})
				// Swal.fire({
        //  icon: 'success',
        //  text: 'Your Proposal has been submitted',
        //  confirmButtonColor: 'blue',
        // });
        // window.location.reload();
			})
			.catch(error => {
				console.log(error)
				console.log('GAGAL')
			})
		}
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		// console.log(this.state.login_status)
		// console.log(this.state.name)
		// console.log(this.state.directorate)
		// console.log(this.state.judulProject)
		// console.log(this.state.nama_anggota1)
		// console.log(this.state.email_anggota1)
		// console.log(this.state.nama_anggota2)
		// console.log(this.state.email_anggota2)
		// console.log(this.state.nama_superior)
		// console.log(this.state.email_superior)
		console.log(this.state.file_uploaded)
		console.log(this.state.ktp_pdf)
		console.log(this.state.ktp_pdf)


		const file_uploaded = this.state.file_uploaded
    const is_file_uploaded = this.state.is_file_uploaded
    // const handleChangeName = this.handleChangeName.bind(this)
    const handleChangeDirectorate = this.handleChangeDirectorate.bind(this)
    const handleChangeJudulProject = this.handleChangeJudulProject.bind(this)
    const handleChangeDeskripsiProject = this.handleChangeDeskripsiProject.bind(this)
    const handleChangeNamaAnggota1 = this.handleChangeNamaAnggota1.bind(this)
    const handleChangeEmailAnggota1 = this.handleChangeEmailAnggota1.bind(this)
    const handleChangeNamaAnggota2 = this.handleChangeNamaAnggota2.bind(this)
    const handleChangeEmailAnggota2 = this.handleChangeEmailAnggota2.bind(this)
    const handleChangeNamaSuperior = this.handleChangeNamaSuperior.bind(this)
    const handleChangeEmailSuperior = this.handleChangeEmailSuperior.bind(this)
    const handleSubmit = this.handleSubmit.bind(this)

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			console.log(this.state.userloggedin.display_name)
			const display_name = this.state.userloggedin.display_name
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'65px'}}>
	              <Container>
									<h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
										Hallo, {this.state.userloggedin.display_name}!
									</h2>
									<div className="title-brand">
	                  <h1 style={{fontSize: '18px', fontWeight:'400', fontFamily: 'Montserrat', marginTop:'15px', color:'#2B678C'}}>
	                    <b>Selamat datang para Innovator!</b>
	                  </h1>

									</div>
	                <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', marginBottom:'10px', fontWeight:'600', fontSize:'16px', color:'#2B678C'}}>
	                	Ayo submit proposal kamu di sini!
	                </h2>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
									<Row>
	                  <Col className="ml-auto mr-auto">
											<Card className="card-register" style ={{
												backgroundColor:'#ffffff',
												margin:'10px',
												minHeight:'0px',
												color:'#333333',
												boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
											}}>
												<FormControl >
													<Typography variant="caption" style={{color:'#aaaaaa'}}>
														Choose proposal (*pdf/jpg/jpeg/png)
													</Typography>
													<Row>
														<Col xs="1" sm="2">
														</Col>
														<Col xs="8" sm="7">
															<input style={{marginLeft:'0px'}}
																// accept="image/*"
																id="contained-button-file"
																placeholder="ID Card"
																name="ktp_pdf"
																type="file"
																required={true}
																onChange={this.handleFileInput.bind(this)}
															/>
														</Col>
														<Col xs="3" sm="1">
														</Col>
													</Row>
												</FormControl>

												<Button
													className='centerobject'
													href="#pablo"
													onClick={e => e.preventDefault()}
													size="sm"
													style={{
														marginBottom:'20px',
														backgroundColor: "#F5B94F",
														borderColor: "#F5B94F",
														color: "#ffffff",
														borderRadius: "20px"
													}}
													onClick={this.handleClickUploadFile.bind(this)}
												>
													Upload Proposal
												</Button>

												<Row>
													<Col lg="12" style={{color:'#999999'}}>
														{this.state.progressStatus}
													</Col>
													{(function() {
															if(file_uploaded.id != null) {
																return(
																	<Col lg="12" style={{marginBottom:'10px'}}>
																		<p style={{margin:'0px', color:'#999999', fontSize:'10px'}}>
																			File uploaded : {file_uploaded.filename}
																		</p>
																		<p style={{margin:'0px', color:'#999999', fontSize:'10px'}}>
																			Success uploaded file to server
																		</p>
																	</Col>
																)
															}
													})()}
												</Row>

												{(function() {
														if(is_file_uploaded) {
															return(
																<Form className="homeGrid">
																	<FormGroup>
																		<Input
																			aria-describedby="emailHelp"
																			id="exampleInputEmail1"
																			placeholder={display_name}
																			type="name"
						                        ></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Directorate *"
																			type="name"
																			onChange={handleChangeDirectorate}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Apa judul projectmu *"
																			type="name"
																			onChange={handleChangeJudulProject}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Deskripsi project *"
																			type="name"
																			onChange={handleChangeDeskripsiProject}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Nama Anggota 1"
																			type="name"
																			onChange={handleChangeNamaAnggota1}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Email Anggota 1"
																			type="name"
																			onChange={handleChangeEmailAnggota1}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Nama Anggota 2"
																			type="name"
																			onChange={handleChangeNamaAnggota2}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Email Anggota 2"
																			type="name"
																			onChange={handleChangeEmailAnggota2}
																		></Input>
																	</FormGroup>

																	<p style={{margin:'0px', color:'#555555', fontSize:'12px', textAlign:'left'}}>
																		Superior
																	</p>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Nama Superior *"
																			type="name"
																			onChange={handleChangeNamaSuperior}
																			style={{marginTop:'10px'}}
																		></Input>
																	</FormGroup>
																	<FormGroup>
																		<Input
																			id="exampleInputPassword1"
																			placeholder="Email Superior *"
																			type="name"
																			onChange={handleChangeEmailSuperior}
																		></Input>
																	</FormGroup>

																	<Button
				                            className="btn-round"
				                            color="default"
				                            type="button"
				                            style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'50%', marginTop:'10px'}}
																		onClick={handleSubmit}
																	>
				                            Submit
				                          </Button>
																</Form>
															)
														}
												})()}

											</Card>
										</Col>
									</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
