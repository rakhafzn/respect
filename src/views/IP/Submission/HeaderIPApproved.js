/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import Swal from 'sweetalert2';
import { Button, Card, CardBody, CardTitle, CardHeader, UncontrolledCollapse,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { makeStyles, Typography, TextField, FormControl } from '@material-ui/core';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),

		})
	}

	componentDidMount(){

	}



  render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)


		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			console.log(this.state.userloggedin.display_name)
			const display_name = this.state.userloggedin.display_name
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'65px'}}>
	              <Container>
									<h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
										Hallo, {this.state.userloggedin.display_name}!
									</h2>
									<div className="title-brand">
	                  <h1 style={{fontSize: '16px', fontWeight:'400', fontFamily: 'Montserrat', marginTop:'15px', color:'#2B678C'}}>
	                    <b>Selamat proposal kamu telah diterima</b>
	                  </h1>

									</div>
	                <h2 className="presentation-subtitle text-center" style={{marginTop:'10px', marginBottom:'10px', fontWeight:'600', fontSize:'16px', color:'#2B678C'}}>
	                	Selamat mengerjakan Improvement Project-nya!
	                </h2>
	                <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', marginBottom:'10px', fontWeight:'600', fontSize:'12px', color:'#2B678C'}}>
	                	Klik dibawah ini untuk submit report dari IP mu
	                </h2>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
										<Row>
											<Col style={{padding:'0px', margin:'0px', width:'100%'}}>
												<Card className="card-register" style ={{
													backgroundColor:'#ffffff',
													margin:'0px',
													padding:'0px',
													minHeight:'0px',
													width:'100%',
													color:'#333333',
													boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
												}}>
													<CardHeader className="card-collapse" id="headingOne" role="tab" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
														<h5 className="mb-0 panel-title" style={{margin:'5px', fontWeight:'600', fontSize:'16px'}}>
															Upload Report IP
															<i className="nc-icon nc-minimal-down" />
														</h5>
													</CardHeader>
													<UncontrolledCollapse toggler="#headingOne">
														<CardBody style={{paddingTop:'0px'}}>
															<div>
																<p>
																	Form Upload Here !
																</p>
															</div>

														</CardBody>
													</UncontrolledCollapse>
												</Card>
											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
