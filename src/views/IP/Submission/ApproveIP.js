/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';
import Datetime from "react-datetime";
import Select from "react-select";
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import Swal from 'sweetalert2';
import Moment from "moment";
import Icon from '@material-ui/core/Icon';

// reactstrap components
import axios from 'axios'
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			name: null,
			directorate: null,
			judulProject: null,
			idea_submitted : [],
      idea_filtered : [],
			idea_choosen : [],
			description_popup : false,
			choosed_detail : null,
			status_idea : null,
			comment_idea : null,
			filename_download : null,
		})
	}

	componentDidMount(){
		// console.log(this.state.user_email)
		//axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?status=eq.review&email_superior=eq.'+this.state.user_email)
		axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?status=eq.review')
		.then(response => {
      function filterArray(array, pname) {
        return array.filter ((element) => {
          return element.name === pname
        })
      }
      const users = [...new Set(response.data.map(x => x.name))]
      console.log(response.data)
      console.log(users)
      var pname2 = users[0]
      console.log(filterArray(response.data, pname2))
			this.setState({
				idea_submitted : response.data,
        idea_filtered : filterArray(response.data, pname2),
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

	handleChangeStatus = event => {
		// console.log(event.target.value)
		this.setState({
			status_idea: event.target.value
		})
	}
	handleChangeComment = event => {
		// console.log(event.target.value)
		this.setState({
			comment_idea: event.target.value
		})
	}

  filterProject = event => {
		// console.log(event)
    function filterArray(array, pname) {
      return array.filter ((element) => {
        return element.name === pname
      })
    }
    // var participantName = event.target.textContent
    var listtask = filterArray(this.state.idea_submitted, event)
    this.setState({
      idea_filtered : listtask,
    })
  }

	handleClickChooseFileDownload = (event,obj) => {
		// console.log(event)
		// console.log(obj)
		this.setState({
			filename_download : event
		})
	}

	handleClickPopUpDescription = (event,target) =>{
		console.log(event)
		function filterArray(array, idea_title) {
      return array.filter ((element) => {
        return element.idea_title === idea_title
      })
    }
    // var participantName = event.target.textContent
    var listtask = filterArray(this.state.idea_submitted, event)
    console.log(listtask)
		// console.log(target);
    this.setState({
      description_popup : true,
			choosed_detail : event,
			idea_choosen : listtask,
    });
	}

	handleSubmit = event =>{
		console.log(event)
		console.log(event.idea_title)
		if(this.state.status_idea === null || this.state.status_idea === "" || this.state.status_idea === "Pilih Status...") {
			Swal.fire({
        icon: 'error',
        text: 'Pilih status approval sebelum update!',
        confirmButtonColor: '#e55555',
      });
		} else {
			const status_ide = this.state.status_idea
			var task = 'approved'
			if (status_ide === 'Revision') {
				task = 'returned to be revised'
			} else if (status_ide === 'Archived') {
				task = 'archived'
			}
			var idea_data = {
				"status": this.state.status_idea,
				"comment": this.state.comment_idea,
			}
			var log_data = {
				"user": this.state.userloggedin.display_name,
				"email": event.email,
				"idea_title": event.idea_title,
				"task": this.state.status_idea,
				"email_superior": event.email_superior,
				"filename": event.filename,
			}
			console.log(idea_data)
			console.log(log_data)
			axios.patch('https://training-api.pti-cosmetics.com/idea_submited_ip?id=eq.'+event.id, idea_data)
			.then(response => {
				console.log('Idea berhasil di approve')
				axios.post('https://training-api.pti-cosmetics.com/idea_task_log', log_data)
				.then(response => {
					console.log('Log berhasil dibuat')
					Swal.fire({
	         icon: 'success',
	         text: 'The proposal has been '+task,
	         confirmButtonColor: 'blue',
	        }).then(function (result) {
					  if (true) {
					    window.location = "/ip-approval-superior";
					  }
					});
				})
				.catch(error => {
					console.log(error)
					console.log('GAGAL')
				})
			})
			.catch(error => {
				console.log(error)
				console.log('GAGAL')
			})
		}
		this.setState({
			description_popup : false,
		})
	}

	closeModal = () => {
		this.setState({
			description_popup : false,
		})
	}

	render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
		console.log(this.state.login_status)
		console.log(this.state.name)
		console.log(this.state.directorate)
		console.log(this.state.judulProject)
		console.log(this.state.status_idea)
		console.log(this.state.comment_idea)
		SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/home" />
      )
    } else {
			var openfile = 'https://testing-bivi-007.pti-cosmetics.com/bivi-api/assets/'+this.state.filename_download
			console.log(openfile)
			const handleClickChooseFileDownload = this.handleClickChooseFileDownload
			const choosed_detail = this.state.choosed_detail
			const handleSubmit = this.handleSubmit.bind(this)
			const handleStatusChange = this.handleChangeStatus.bind(this)
			const handleCommentChange = this.handleChangeComment.bind(this)
			var chosen_task = this.state.idea_choosen[0]
			var idea_submitter_name = null
			var idea_id = null
			var idea_description = null
			var idea_date = null
			var idea_file = null
			var idea_timestamp = new Date()
      var today = new Date()
			var renderers = {
			  renderDay: function( props, currentDate, selectedDate ){
			    return <td {...props}>{currentDate.date() }</td>;
			  },
			  renderMonth: function( props, month, year, selectedDate){
			    return <td {...props}>{ month }</td>;
			  },
			  renderYear: function( props, year, selectedDate ){
			    return <td {...props}>{ year % 100 }</td>;
			  }
			}
      var listproject = this.state.idea_submitted
      const users = [...new Set(listproject.map(x => x.name))]
      var listproject2 = this.state.idea_filtered
      const titles2 = [...new Set(listproject2.map(x => x.idea_title))]
      console.log(users)
      console.log(users[0])
      console.log(titles2)
			console.log(chosen_task)
			if (!chosen_task) {
				console.log("masih kosong")
			} else {
				idea_submitter_name = chosen_task.name
				idea_id = chosen_task.id
				idea_description = chosen_task.idea_desc
				idea_timestamp = chosen_task.timestamp
				idea_file = chosen_task.filename
				idea_date = Moment(idea_timestamp).format("D MMM YYYY")
				console.log(idea_date)
			}
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', padding:'0px', marginBottom:'20px', marginTop:'120px'}}
	            className="section"
	          >
	            <div style={{marginTop:'40px'}}>
	              <Container>
								<h2 className="presentation-subtitle text-center" style={{marginTop:'20px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
									Participant
								</h2>
								<Swiper
						      spaceBetween={7}
						      slidesPerView={3}
									pagination={{clickable:true}}
						      onSlideChange={() => console.log('slide change')}
						      onSwiper={(swiper) => console.log(swiper)}
									style={{margin:'0px',padding:'5px'}}
						    >
								{users.map((row)=> (
									<SwiperSlide>
										<Card
											data-background="color"
											data-color="white"
											style={{margin:'0px', backgroundColor:'#ffffff',
											color:'#333333',
											boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.05), 0 1px 5px 0 rgba(0, 0, 0, 0.05)',}}
											onClick={this.filterProject.bind(this, row)}
										>
											<CardBody className="text-center" style={{padding:'5px', margin:'10px'}}>
												<img style={{width:'30px', margin:'3px'}}
													alt="..."
													src={require("assets/img/Icon/Icons/Participant.png")}
												/>
												<h4 className="centerobject" style={{
													padding:'0px',
													margin: '10px',
													textAlign: 'center',
													fontFamily: 'Montserrat',
													fontWeight:'600',
													fontSize: '12px',
													color:'#2B678C',
													height:'30px'
												}}>
													{row}
												</h4>
											</CardBody>
										</Card>
										<h3 className="centerobject" style={{
											padding:'0px',
											margin: '1px',
											textAlign: 'center',
											fontFamily: 'Montserrat',
											fontWeight:'600',
											fontSize: '12px',
											color:'#2B678C',
											height:'30px'}}> </h3>
									</SwiperSlide>
								))}
						    </Swiper>


									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center', marginBottom:'100px'}}>
										<Row>
		                  <Col className="ml-auto mr-auto">
                        <br></br>

                        <h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
                          Review Proposal
                        </h2>

                        {titles2.map((row)=> (
  											<Card className="card-timeline"
														style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px',
														color:'#333333',
														boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',}}
														onClick={this.handleClickPopUpDescription.bind(this, row)}>
  												<CardBody style ={{backgroundColor:'#ffffff', margin:'2px', minHeight:'0px'}}>
                            <h4 className="centerobject" style={{
                              padding:'0px',
                              margin: '0px',
                              textAlign: 'center',
                              fontFamily: 'Montserrat',
                              fontWeight:'600',
                              fontSize: '12px',
                              color:'#2B678C'
                            }}>
                              {row}
                            </h4>
  												</CardBody>
  											</Card>
                        ))}

											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

					<Dialog
						open={this.state.description_popup}
						closeOnDocumentClick
						onClose={this.closeModal}
					>
						{(function(){
								return(
									<div>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat', fontWeight:'600', fontSize: '14px', color:'#2B678C'
										}}>
											Approval
										</p>
										<h4 className="centerobject" style={{
											paddingTop:'5px',
											paddingRight:'20px',
											paddingLeft:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'600',
											fontSize: '16px',
											color:'#2B678C'
										}}>
											{choosed_detail}
										</h4>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
										<Icon className="fa fa-calendar" fontSize="small" style={{
											marginTop:'5px',
											marginRight:'10px',
										}}/>
											{idea_date}
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											textAlign: 'left',
											fontWeight:'400',
											fontFamily: 'Montserrat',
											fontSize: '12px',
											color:'#8F8F8F'
										}}>
											Description
										</p>
										<p className="centerobject" style={{
											paddingLeft:'20px',
											paddingTop:'5px',
											paddingRight:'20px',
											textAlign: 'left',
											fontFamily: 'Montserrat',
											fontWeight:'500',
											fontSize: '12px',
											color:'#2B678C'
										}}>
											{idea_description}
										</p>
										<div className="centerobject" style={{widht:'100%', textAlign: 'center', marginTop:'10px'}}>
											<a
												href={openfile}
												target="_blank"
											>
												<Button
													className="btn-round"
													color="default"
													size='sm'
													onClick={handleClickChooseFileDownload.bind(this,idea_file)}
													style={{
														paddingLeft:'20px',
														paddingTop:'5px',
														paddingRight:'20px',
														textAlign: 'center',
														fontFamily: 'Montserrat',
														fontWeight:'500',
														fontSize: '12px',
													}}
												>
													<h4 className="centerobject" style={{
														padding:'0px',
														textAlign: 'center',
														marginTop: '0px',
														marginBottom: '0px',
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '8px',
														color: '#ffffff'
													}}>
														download proposal
													</h4>
												</Button>
											</a>
										</div>
										<Form style={{
											paddingLeft:'20px',
											paddingTop:'10px',
											paddingRight:'20px',
											fontWeight:'400'
										}}>
							        <FormGroup>
							          <label htmlFor="exampleFormControlSelect1" style={{textAlign: 'left', fontFamily: 'Montserrat',
												fontWeight:'600',
												fontSize: '12px', color:'#2B678C'}}>
												Approval Status
												</label>
							          <Input id="exampleFormControlSelect1"
														placeholder="Pilih Status..."
														type="select"
														onChange={handleStatusChange}
														style={{
																color:'#2B678C',
																fontFamily: 'Montserrat',
																fontWeight:'700',
																fontSize: '12px'}}>
													<option disabled selected></option>
													<option style={{color:'#28B15F', fontFamily: 'Montserrat', fontWeight:'700', fontSize: '12px'}}>Approve</option>
							            <option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'700', fontSize: '12px'}}>Revision</option>
													<option style={{color:'#2B678C', fontFamily: 'Montserrat', fontWeight:'700', fontSize: '12px'}}>Archived</option>
							          </Input>
							        </FormGroup>
							        <FormGroup>
							          <label htmlFor="exampleFormControlTextarea1" style={{textAlign: 'left', fontFamily: 'Montserrat',
												fontWeight:'600',
												fontSize: '12px', color:'#2B678C'}}>
							           Komentar
							          </label>
							          <Input
													id="exampleInputPassword1"
													placeholder="Komentar..."
													type="textarea"
													style={{ height: 100, fontFamily: 'Montserrat',
													fontWeight:'400',
													fontSize: '12px', color:'#2B678C'}}
													onChange={handleCommentChange}>
												</Input>
							        </FormGroup>
							      </Form>
									</div>
								)
						})()}

						<div className="centerobject" style={{widht:'100%', textAlign: 'center', marginTop:'10px'}}>
							<Button
								className="centerobject btn-round"
								color="default"
								type="button"
								style={{backgroundColor:'#F1A124', borderColor:'#F1A124', width:'50%', marginBottom:'10px'}}
								onClick={this.handleSubmit.bind(this, chosen_task)}
							>
								Update
							</Button>
						</div>

					</Dialog>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
