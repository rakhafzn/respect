/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';
import Datetime from "react-datetime";
import Dialog from '@material-ui/core/Dialog';
import Icon from '@material-ui/core/Icon';
import Moment from "moment";
// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle, CardHeader, UncontrolledCollapse, InputGroup, InputGroupAddon,
InputGroupText,	Form, FormGroup, FormText, Label, Input, Container, Row, Col, Modal } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import * as emailjs from 'emailjs-com'
import Swal from 'sweetalert2';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			name: null,
			idea_submitted : [],
			idea_filtered : null,
			description_popup : false,
			choosed_detail : null,
			log_urs : [],
			directorate: "-",
			judulProject: "-",
			businessOwner: "-",
			priority: "-",
			entity: "-",
			impactedApps: "-",
			goliveDate: "-",
			ref: "-",
			objective: "-",
			background: "-",
			scope: "-",
			deliverable: "-",
			current_state: "-",
			desired_state: "-",
			quantitative_benefit: "-",
			qualitative_benefit: "-",
			risk_assumption: "-",
			success_matrix: "-",
			uat_scenario: "-",
			business_owner: [],
		})
	}

	componentDidMount(){
		// console.log(this.state.user_email)
		axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_form?or=(requester_email.eq.'+this.state.user_email+')')
		.then(response => {
			// console.log(response.data)
			this.setState({
				idea_submitted : response.data
			})
		})
		.catch(error => {
			// console.log(error)
			console.log('GAGAL')
		})
	}

	handleClickPopUpDescription = (event,target) =>{
		console.log('https://sfa-api-testing.pti-cosmetics.com/urs_log?ref.eq.'+event.ref)
		axios.get('https://sfa-api-testing.pti-cosmetics.com/urs_log?or=(reference.eq.'+event.ref+')')
		.then(response => {
			
			function filterArray(array, project_name) {
				return array.filter ((element) => {
					return element.project_name === project_name
				})
			}
			// var participantName = event.target.textContent
			var listtask = filterArray(this.state.idea_submitted, event)
			// console.log('UUUUUUUUUUUU')
				// console.log(target);
			this.setState({
					description_popup : true,
					choosed_detail : event,
					idea_choosen : listtask,
					log_urs : response.data,
					businessOwner: event.business_owner,
					uat_scenario: event.uat_scenario,
					directorate: event.directorate,
					judulProject: event.project_name,
					priority: event.priority,
					entity: event.entity,
					impacted_apps: event.impacted_apps,
					golive_date: event.golive_date,
					objective: event.objective,
					background: event.background,
					scope: event.scope,
					deliverable: event.deliverable,
					current_state: event.current_state,
					desired_state: event.desired_state,
					quantitative_benefit: event.quantitative_benefit,
					risk_assumption: event.risk_assumption,
					success_matrix: event.success_matrix,
					business_owner: event.business_owner,
					urs_id: event.id,
					ref: event.ref
			});
		})
		.catch(error => {
			// console.log(error)
			console.log('GAGAL')
		})
		
	}

// 	

  closeModal = () => {
	this.setState({
		description_popup : false,
	})
}
	
	handleChangeObjective = event => {
		// console.log(event.target.value)
		this.setState({
			objective: event.target.value
		})
	}
	handleChangeBackground = event => {
		// console.log(event.target.value)
		this.setState({
			background: event.target.value
		})
	}
	handleChangeScope = event => {
		// console.log(event.target.value)
		this.setState({
			scope: event.target.value
		})
	}
	handleChangeDeliverable = event => {
		// console.log(event.target.value)
		this.setState({
			deliverable: event.target.value
		})
	}
	handleChangeCurrentState = event => {
		// console.log(event.target.value)
		this.setState({
			current_state: event.target.value
		})
	}
	handleChangeDesiredState = event => {
		// console.log(event.target.value)
		this.setState({
			desired_state: event.target.value
		})
	}
	handleChangeQuantitativeBenefit = event => {
		// console.log(event.target.value)
		this.setState({
			quantitative_benefit: event.target.value
		})
	}
	handleChangeQualitativeBenefit = event => {
		// console.log(event.target.value)
		this.setState({
			qualitative_benefit: event.target.value
		})
	}
	handleChangeRiskAssumption = event => {
		// console.log(event.target.value)
		this.setState({
			risk_assumption: event.target.value
		})
	}
	handleChangeSuccessfulMatrix = event => {
		// console.log(event.target.value)
		this.setState({
			success_matrix: event.target.value
		})
	}
	handleChangeUatScenario = event => {
		// console.log(event.target.value)
		this.setState({
			uat_scenario: event.target.value
		})
	}

	handleDraft = event =>{
		var urs_data = {
				"status": "draft",
				"background": this.state.background,
				"objective": this.state.objective,
				"scope": this.state.scope,
				"deliverable": this.state.deliverable,
				"current_state": this.state.current_state,
				"desired_state": this.state.desired_state,
				"quantitative_benefit": this.state.quantitative_benefit,
				"qualitative_benefit": this.state.qualitative_benefit,
				"risk_assumption": this.state.risk_assumption,
				"success_matrix": this.state.success_matrix,
				"uat_scenario": this.state.uat_scenario,
			}
      
			axios.patch('https://sfa-api-testing.pti-cosmetics.com/urs_form?id=eq.'+this.state.urs_id, urs_data)
			.then(response => {
				console.log('URS saved as draft!')
				this.setState({
					description_popup : false,
				  })
				Swal.fire({
                    icon: 'success',
                    text: 'Change Request saved as draft',
                    confirmButtonColor: 'blue',
                }).then(function (result) {
					  if (true) {
					    window.location = "/check";
					  }
					});
				})
				.catch(error => {
					console.log(error)
					console.log('GAGAL')
				})
				
		
	}

	handleSubmit = event =>{
		if( this.state.background === null || this.state.objective === null
            || this.state.scope === null || this.state.deliverable === null || this.state.current_state === null
            || this.state.desired_state === null || this.state.quantitative_benefit === null || this.state.qualitative_benefit === null
            || this.state.risk_assumption === null || this.state.success_matrix === null || this.state.uat_scenario === null
            || this.state.background === "-" || this.state.objective === "-"
            || this.state.scope === "-" || this.state.deliverable === "-" || this.state.current_state === "-"
            || this.state.desired_state === "-" || this.state.quantitative_benefit === "-" || this.state.qualitative_benefit === "-"
            || this.state.risk_assumption === "-" || this.state.success_matrix === "-" || this.state.uat_scenario === "-") {
				Swal.fire({
					icon: 'error',
					text: 'Complete your form!',
					confirmButtonColor: '#e55555',
				});
		} else {
			var urs_data = {
				"status": "request",
				"background": this.state.background,
				"objective": this.state.objective,
				"scope": this.state.scope,
				"deliverable": this.state.deliverable,
				"current_state": this.state.current_state,
				"desired_state": this.state.desired_state,
				"quantitative_benefit": this.state.quantitative_benefit,
				"qualitative_benefit": this.state.qualitative_benefit,
				"risk_assumption": this.state.risk_assumption,
				"success_matrix": this.state.success_matrix,
				"uat_scenario": this.state.uat_scenario,
			}
			var log_data ={
				"reference": this.state.ref,
				"action": "submit",
				"email": this.state.user_email,
				"urs": this.state.judulProject,
			  }
			axios.patch('https://sfa-api-testing.pti-cosmetics.com/urs_form?id=eq.'+this.state.urs_id, urs_data)
			.then(response => {
				console.log('URS berhasil di submit')
				this.setState({
					description_popup : false,
				  })
				axios.post('https://sfa-api-testing.pti-cosmetics.com/urs_log', log_data)
				  .then(response2=>{
					emailjs.send("service_615ys9m","template_nqncl9k",{
					  from_name: this.state.userloggedin.display_name,
					  message: this.state.userloggedin.display_name + " submitted new URS: " + this.state.judulProject + ". Please check it on Respect!",
					  to_email: this.state.businessOwner,
					  }, "user_h1mWZcdzQiHbtquLsKZ5y"
					  );
					Swal.fire({
					  icon: 'success',
					  text: 'Change Request is submitted',
					  confirmButtonColor: 'blue',
					}).then(function (result) {
					  if (true) {
						window.location = "/check";
					  }
					});
				  })
				})
				.catch(error => {
					console.log(error)
					console.log('GAGAL')
				})
			}
		
	}



	render(){
		console.log(this.state.idea_submitted)
		
		const handleChangeObjective = this.handleChangeObjective.bind(this)
		const handleChangeBackground = this.handleChangeBackground.bind(this)
		const handleChangeScope = this.handleChangeScope.bind(this)
		const handleChangeDeliverable = this.handleChangeDeliverable.bind(this)
		const handleChangeCurrentState = this.handleChangeCurrentState.bind(this)
		const handleChangeDesiredState = this.handleChangeDesiredState.bind(this)
		const handleChangeQuantitativeBenefit = this.handleChangeQuantitativeBenefit.bind(this)
		const handleChangeQualitativeBenefit = this.handleChangeQualitativeBenefit.bind(this)
		const handleChangeRiskAssumption = this.handleChangeRiskAssumption.bind(this)
		const handleChangeSuccessfulMatrix = this.handleChangeSuccessfulMatrix.bind(this)
		const handleChangeUatScenario = this.handleChangeUatScenario.bind(this)
		const handleSubmit = this.handleSubmit.bind(this)
		const handleDraft = this.handleDraft.bind(this)
		
		// const handleSubmit = this.handleSubmit.bind(this)
		// const handleDraft = this.handleDraft.bind(this)
		var chosen_task = this.state.idea_filtered
		var rev_idea_title = null
		var choosed_detail = this.state.choosed_detail
		if (chosen_task) {
			rev_idea_title = chosen_task.idea_title
		}
		let qualitative_benefit,status, directorate, judulProject, businessOwner, priority, entity
		let log_urs, impacted_apps, golive_date, objective, background, scope, deliverable, current_state
		let desired_state,	quantitative_benefit, risk_assumption, success_matrix, uat_scenario, business_owner
		
		if (choosed_detail) {
			log_urs = this.state.log_urs
			directorate =  this.state.directorate
			judulProject =  this.state.judulProject
			priority =  this.state.priority
			entity =  this.state.entity
			impacted_apps =  this.state.impacted_apps
			golive_date =  this.state.golive_date
			// ref =  choosed_detail.ref
			objective =  this.state.objective
			background =  this.state.background
			scope =  this.state.scope
			deliverable = choosed_detail.deliverable	
			current_state = this.state.current_state
			desired_state = this.state.desired_state
			quantitative_benefit = this.state.quantitative_benefit
			qualitative_benefit = this.state.qualitative_benefit
			risk_assumption = this.state.risk_assumption
			success_matrix = this.state.success_matrix
			uat_scenario = this.state.uat_scenario
			business_owner = choosed_detail.business_owner
			status = choosed_detail.status		
		}
		
		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', padding:'0px', marginBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'10px',marginBottom:'100px'}}>
	              <Container>
									{/* <div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}> */}
										{/* <Row> */}
											{/* <Col style={{padding:'0px', margin:'0px', width:'100%'}}> */}
												<Card  style ={{
													backgroundColor:'#ffffff',
													margin:'0px',
													padding:'0px',
													minHeight:'0px',
													width:'100%',
													color:'#333333',
													boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
												}}>
	                        <CardHeader className="card-collapse" id="headingOne" role="tab" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
	                          <h5 className="mb-0 panel-title" style={{margin:'5px', fontWeight:'600', fontSize:'16px'}}>
                              Check your CR Status here..
                              <i className="nc-icon nc-minimal-down" />
	                          </h5>
	                        </CardHeader>
	                        {/* <UncontrolledCollapse toggler="#headingOne"> */}
	                          <CardBody style={{paddingTop:'0px'}}>
															<div>
																<Row>
																	<Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>ID</b>
																		</h4>
																	</Col>
																	<Col xs="6" sm="6" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px', textAlign:'left'}}>
																			<b>CR Title</b>
																		</h4>
																	</Col>
																	<Col xs="2" sm="2" style={{padding:'0px'}}>
																		<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																			<b>Status</b>
																		</h4>
																	</Col>
																</Row>
															</div>
															{this.state.idea_submitted.map((row)=> (
																<div>
																	<Row onClick={this.handleClickPopUpDescription.bind(this, row)}>
																		<Col xs="2" sm="2" style={{padding:'0px'}}>
																			<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'200', margin:'5px', padding:'0px'}}>
																				<b>{row.id}</b>
																			</h4>
																		</Col>
																		<Col xs="6" sm="6" style={{padding:'0px'}}>
																			<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'200', margin:'5px', padding:'0px', textAlign:'left'}}>
																				<b>{row.project_name}</b>
																			</h4>
																		</Col>
																		<Col xs="2" sm="2" >
																			{(function(){
																				if (row.status === 'request') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#f0ad4e'}}>
																							Submitted
																						</h4>
																					)
																				} else if (row.status === 'approved1') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#5cb85c'}}>
																							Approved by Business Owner
																						</h4>
																					)
																				} else if (row.status === 'approved2') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#5cb85c'}}>
																							Reviewed by IT Business Partner
																						</h4>
																					)
																				} else if (row.status === 'approved3') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#5cb85c'}}>
																							Assigned to Developer
																						</h4>
																					)
																				} else if (row.status === 'assessed') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#5cb85c'}}>
																							On Development
																						</h4>
																					)
																				} else if (row.status === 'rejected') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#000000', color:'#ffffff'}}>
																							Rejected
																						</h4>
																					)
																				} else if (row.status === 'draft') {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#ffffff', color:'#000000'}}>
																							Draft
																						</h4>
																					)
																				} else {
																					return(
																						<h4 style={{fontSize: '12px', fontFamily: 'Poppins', fontWeight:'200', margin:'2px', padding:'3px', backgroundColor:'#0275d8', color:'#ffffff'}}>
																							etc.
																						</h4>
																					)
																				}
																			})()}
																		</Col>
																	</Row>
																</div>
															))}
	                          </CardBody>
	                        {/* </UncontrolledCollapse> */}
	                      </Card>
						  
						
								</Container>
					
	            </div>
	          </div>
	        </div>

			<Dialog
						open={this.state.description_popup}
						closeOnDocumentClick
						onClose={this.closeModal}
						
					>
						
									<div> 
										{function(){ console.log(choosed_detail)}}
										 <CardBody style={{padding:'5px', margin:'10px'}}>

											<Form className="homeGrid">
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Project Name
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {judulProject}
														type="name"
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Business Owner
													</Label>
													<Input
														aria-describedby="emailHelp"
														id="exampleInputEmail1"
														value = {business_owner}
														type="name"
													>
													
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Directorate
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {directorate}
														type="name"
														
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Priority
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {priority}
														type="name"
														
													>
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Entity
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {entity}
														type="name"
													>
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Impacted Application
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {impacted_apps}
														type="name"
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'45px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Expected Go Live
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {golive_date}
														type="date"
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Background
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {background}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeBackground}
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Objective
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {objective}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeObjective}
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Project Scope
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {scope}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeScope}
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Project Deliverable
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {deliverable}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeDeliverable}
													>
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
												<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Current State
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {current_state}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeCurrentState}
													>
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'45px'}}>
												<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Desired State
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {desired_state}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeDesiredState}
													>
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Quantitative Benefit
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {quantitative_benefit}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeQuantitativeBenefit}
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Qualitative Benefit
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {qualitative_benefit}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeQualitativeBenefit}
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Risk and Assumptions
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {risk_assumption}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeRiskAssumption}
													></Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
													<Label style={{color:'#2B678C', fontSize:'16px'}}>
														Successful Matrix
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {success_matrix}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeSuccessfulMatrix}
													>
													</Input>
												</FormGroup>
												<FormGroup style={{marginBottom:'25px'}}>
												<Label style={{color:'#2B678C', fontSize:'16px'}}>
														UAT Scenario
													</Label>
													<Input
														id="exampleInputPassword1"
														value = {uat_scenario}
														type="textarea"
														style={{ 
														height: 100, 
														fontFamily: 'Montserrat',
														fontWeight:'400',
														fontSize: '16px', 
														color:'#2B678C'
														}}
														onChange={handleChangeUatScenario}
													>
													</Input>
												</FormGroup>
													{(function(){
														if (status === 'draft' || status === 'rejected' ) {
															return(	
															<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
																<Button
																	className="btn-round"
																	color="default"
																	type="button"
																	style={{
																		backgroundColor:'#2B678C', 
																		borderColor:'#2B678C',
																		width:'50%', 
																		marginTop:'10px'}}
																	onClick={handleDraft}
																>
																	Save as Draft
																</Button>
																<Button
																	className="btn-round"
																	color="default"
																	type="button"
																	style={{
																		backgroundColor:'#F1A124', 
																		borderColor:'#F1A124',
																		width:'50%', 
																		marginTop:'10px'}}
																	onClick={handleSubmit}
																>
																	Submit
																</Button>
															</div>
															)
														}
													})()}
												
											</Form>
										</CardBody>
										<CardHeader className="card-collapse" id="headingOne" role="tab" style ={{backgroundColor:'#ffffff', margin:'10px', minHeight:'0px'}}>
											<h5 className="mb-0 panel-title" style={{margin:'5px', fontWeight:'600', fontSize:'16px'}}>
											Log Request
											<i className="nc-icon nc-minimal-down" />
											</h5>
										</CardHeader>
										<CardBody style={{padding:'5px', margin:'10px'}}>
											<div>
												
												<Row>
													<Col xs="4" sm="4" style={{padding:'0px'}}>
														<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
															<b>Date</b>
														</h4>
													</Col>
													<Col xs="8" sm="8" style={{padding:'0px'}}>
															<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px', textAlign:'left'}}>
																<b>Description</b>
															</h4>
													</Col>
												</Row>
												 {this.state.log_urs.map((row)=> (
													<Row>
														<Col xs="4" sm="4" style={{padding:'0px'}}>
															<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px'}}>
																<b>{ Moment(row.write_date).format("D MMM YYYY")}</b>
															</h4>
														</Col>
														<Col xs="8" sm="8" style={{padding:'0px'}}>
																<h4 style={{fontSize: '14px', fontFamily: 'Poppins', fontWeight:'400', margin:'5px', padding:'0px', textAlign:'left'}}>
																	<b>{row.email + ' ' + row.action + ' ' + row.urs}</b>
																</h4>
														</Col>
													</Row> 
												  ))}
												
											</div>
										</CardBody>
									</div>
								

						

					</Dialog>


	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
