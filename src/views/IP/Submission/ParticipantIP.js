/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';
import Datetime from "react-datetime";
import Grid from '@material-ui/core/Grid';

// reactstrap components
import axios from 'axios'
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
			name: null,
			directorate: null,
			judulProject: null,
			idea_submitted : [],
		})
	}

	componentDidMount(){
		// console.log(this.state.user_email)
		axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?status=eq.review&email_superior=eq.'+this.state.user_email)
		.then(response => {
			console.log(response.data)
			this.setState({
				idea_submitted : response.data
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  cekUser = event => {
    var listproject = this.state.idea_submitted
    const users = [...new Set(listproject.map(x => x.name))]
    console.log(this.state.idea_submitted)
    console.log(listproject)
    console.log(users)
	}

  cekProject = event => {
    function filterArray(array, pname) {
      return array.filter ((element) => {
        return element.name === pname
      })
    }
    var participantName = event
    var listtask = filterArray(this.state.idea_submitted, participantName)
    console.log(event)
    console.log(participantName)
    console.log(listtask)
  }

	render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
		console.log(this.state.login_status)
		console.log(this.state.name)
		console.log(this.state.directorate)
		console.log(this.state.judulProject)
		SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
      var today = new Date()
			var renderers = {
			  renderDay: function( props, currentDate, selectedDate ){
			    return <td {...props}>{currentDate.date() }</td>;
			  },
			  renderMonth: function( props, month, year, selectedDate){
			    return <td {...props}>{ month }</td>;
			  },
			  renderYear: function( props, year, selectedDate ){
			    return <td {...props}>{ year % 100 }</td>;
			  }
			}
      var listproject = this.state.idea_submitted
      const users = [...new Set(listproject.map(x => x.name))]
      console.log(users)
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', padding:'0px', marginBottom:'20px'}}
	            className="section"
	          >
	            <div style={{marginTop:'10px'}}>
	              <Container>
								<h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
									Participant
								</h2>
								<Swiper
						      spaceBetween={7}
									pagination={{clickable:true}}
						      slidesPerView={3}
						      onSlideChange={() => console.log('slide change')}
						      onSwiper={(swiper) => console.log(swiper)}
									style={{margin:'0px',padding:'5px'}}
						    >
								{users.map((row)=> (
									<SwiperSlide>
										<Card
											data-background="color"
											data-color="white"
											style={{margin:'0px', backgroundColor:'#ffffff',
											color:'#333333',
											boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.05), 0 1px 5px 0 rgba(0, 0, 0, 0.05)',}}
											onClick={this.cekProject.bind(this, row)}
										>
											<CardBody className="text-center" style={{padding:'5px', margin:'10px'}}>
												<img style={{width:'32px', margin:'5px'}}
													alt="..."
													src={require("assets/img/Icon/Icons/Participant.png")}
												/>
												<h4 className="centerobject" style={{
													padding:'0px',
													margin: '10px',
													textAlign: 'center',
													fontFamily: 'Montserrat',
													fontWeight:'600',
													fontSize: '12px',
													color:'#2B678C',
													height:'30px'
												}}>
													{row}
												</h4>
											</CardBody>
										</Card>
										<h3 className="centerobject" style={{
											padding:'0px',
											margin: '1px',
											textAlign: 'center',
											fontFamily: 'Montserrat',
											fontWeight:'600',
											fontSize: '12px',
											color:'#2B678C',
											height:'30px'}}> </h3>
									</SwiperSlide>
								))}
						    </Swiper>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center', marginBottom:'100px'}}>
										<Row>
		                  <Col className="ml-auto mr-auto">
                        <br></br>

                        <h2 className="presentation-subtitle text-center" style={{marginTop:'0px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
                          Timeline
                        </h2>

  											<Card className="card-timeline" style ={{backgroundColor:'#ffffff',
												margin:'10px',
												minHeight:'0px',
												color:'#333333',
												boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',}}>
  												<CardBody style ={{backgroundColor:'#ffffff', margin:'5px', minHeight:'0px'}}>
  													<Datetime
  														input={false}
  														timeFormat={false}
  														renderDay={ renderers.renderDay }
  														renderMonth={ renderers.renderMonth }
  														renderYear={ renderers.renderYear }
  														value={today}
  													/>
  												</CardBody>
  											</Card>

											</Col>
										</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
