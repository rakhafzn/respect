/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import Swal from 'sweetalert2';
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { makeStyles, Typography, TextField, FormControl } from '@material-ui/core';
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			user_email: cookies.get('user_email'),
      id_revisi: cookies.get('revision_id'),
			name: null,
			judulProject: null,
			deskripsiProject: null,
      ktp_pdf : null,
      progressStatus: null,
      file_uploaded: {},
      is_file_uploaded: false,
      idea_submitted: [],
		})
	}

	componentDidMount(){
    axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?id=eq.'+this.state.id_revisi)
		.then(response => {
      console.log(response.data)
			this.setState({
				idea_submitted : response.data,
				judulProject : response.data[0].idea_title,
				deskripsiProject : response.data[0].idea_desc,
			})
		})
		.catch(error => {
			console.log(error)
			console.log('GAGAL')
		})
	}

	handleChangeJudulProject = event => {
		// console.log(event.target.value)
		this.setState({
			judulProject: event.target.value
		})
	}
	handleChangeDeskripsiProject = event => {
		// console.log(event.target.value)
		this.setState({
			deskripsiProject: event.target.value
		})
	}

	handleFileInput = event => {
    console.log('this is input file')
    this.setState({[event.target.name]: event.target.files[0]})
    console.log(event.target.name)
    console.log(event.target.files[0])
  }

	handleClickUploadFile = event => {
    if (this.state.ktp_pdf != null) {
      console.log('Upload file siap disimpan')
      var fd = new FormData();
      fd.append('ktp_pdf', this.state.ktp_pdf);

      const options={
        onUploadProgress: (progressEvent) => {
          const{loaded,total} = progressEvent;
          let percent = Math.floor((loaded*100)/total)
          console.log(`${loaded}kb of ${total}kb | ${percent}%`);
          this.setState({
            progressStatus : `${loaded} kb of ${total} kb | ${percent}%`
          })
          if(percent < 100){
            this.setState({ uploadPercentage: percent})
          }
        }
      }
			// simpan di 10.3.181.123
      axios.post('https://testing-bivi-007.pti-cosmetics.com/bivi-api/index.php/api/docprofileip', fd, options)
      .then(response => {
        console.log(response.data)
        this.setState({uploadPercentage:100}, ()=>{
          setTimeout(()=>{
            this.setState({uploadPercentage:0})
          },1000)
        })
        var status = response.data.status
        console.log(response.status)
        if(status === true){
          axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?order=id.desc&limit=1')
          .then(response => {
            console.log(response.data)
            this.setState({
              file_uploaded : response.data[0],
				      is_file_uploaded: true,
            })
            Swal.fire({
             icon: 'success',
             text: 'Your file has been uploaded',
             confirmButtonColor: 'blue',
            });
          })
          .catch(error => {
            console.log(error)
          })
        }
        // this.props.history.goBack();
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          text: 'Form cant be submitted!',
          confirmButtonColor: '#e55555',
        });
        // this.props.history.goBack();
        console.log(error.response)
      })
    } else {
      Swal.fire({
        icon: 'error',
        text: 'Upload File Failed!',
        confirmButtonColor: '#e55555',
      });
    }
  }

	handleSubmit = event =>{
		const idea_submit = this.state.idea_submitted[0]
		if(this.state.deskripsiProject === null || this.state.judulProject === null) {
			Swal.fire({
        icon: 'error',
        text: 'Complete your form!',
        confirmButtonColor: '#e55555',
      });
		} else {
			var idea_data = {
				"idea_title": this.state.judulProject,
				"idea_desc": this.state.deskripsiProject,
				"filename": this.state.file_uploaded.filename,
				"status": "review",
			}
			var log_data = {
				"user": this.state.userloggedin.display_name,
				"email": idea_submit.email,
				"idea_title": idea_submit.idea_title,
				"task": "update",
				"email_superior": idea_submit.email_superior,
				"filename": idea_submit.filename,
			}
			console.log(idea_data)
			console.log(log_data)
			axios.patch('https://training-api.pti-cosmetics.com/idea_submited_ip?id=eq.'+this.state.id_revisi, idea_data)
			.then(response => {
				console.log('Idea berhasil di submit')
				axios.post('https://training-api.pti-cosmetics.com/idea_task_log', log_data)
				.then(response => {
					console.log('Log berhasil dibuat')
					Swal.fire({
	         icon: 'success',
	         text: 'Your Proposal has been updated',
	         confirmButtonColor: 'blue',
	        }).then(function (result) {
					  if (true) {
							const cookies = new Cookies();
							cookies.remove('revision_id', {path: '/'});
					    window.location = "/ip";
					  }
					});
				})
				.catch(error => {
					console.log(error)
					console.log('GAGAL')
				})
			})
			.catch(error => {
				console.log(error)
				console.log('GAGAL')
			})
		}
	}

  render(){
		// #2B678C
		// <div className="type">1.0</div>
		// console.log(this.state.login_status)
		// console.log(this.state.name)
		// console.log(this.state.directorate)
		// console.log(this.state.judulProject)
		// console.log(this.state.nama_anggota1)
		// console.log(this.state.email_anggota1)
		// console.log(this.state.nama_anggota2)
		// console.log(this.state.email_anggota2)
		// console.log(this.state.nama_superior)
		// console.log(this.state.email_superior)
		console.log(this.state.file_uploaded.filename)
		console.log(this.state.ktp_pdf)
    console.log(this.state.id_revisi)
    console.log(this.state.idea_submitted)
		console.log(this.state.judulProject)
		console.log(this.state.deskripsiProject)

		const file_uploaded = this.state.file_uploaded
    const is_file_uploaded = this.state.is_file_uploaded
    // const handleChangeName = this.handleChangeName.bind(this)
    const handleChangeJudulProject = this.handleChangeJudulProject.bind(this)
    const handleChangeDeskripsiProject = this.handleChangeDeskripsiProject.bind(this)
    const handleSubmit = this.handleSubmit.bind(this)
    const idea_revision = this.state.idea_submitted[0]
    console.log(idea_revision)
    var idea_title_rev = "Judul Project..."
    var idea_desc_rev = "Deskripsi Project..."
    if (idea_revision) {
      console.log(idea_revision)
      idea_title_rev = idea_revision.idea_title
      idea_desc_rev = idea_revision.idea_desc
      console.log(idea_title_rev)
      console.log(idea_desc_rev)
    }

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			console.log(this.state.userloggedin.display_name)
			const display_name = this.state.userloggedin.display_name
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', paddingBottom:'10px'}}
	            className="section"
	          >
	            <div style={{marginTop:'60px', marginBottom:'20px'}}>
	              <Container>
									<h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'14px', color:'#2B678C'}}>
										Hallo, {this.state.userloggedin.display_name}!
									</h2>

	                <h2 className="presentation-subtitle text-center" style={{marginTop:'30px', fontWeight:'600', fontSize:'16px', color:'#2B678C'}}>
	                	Revisi Proposal
	                </h2>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex'}}>
									<Row>
	                  <Col className="ml-auto mr-auto">
											<Card className="card-register" style ={{
												backgroundColor:'#ffffff',
												margin:'10px',
												minHeight:'0px',
												color:'#333333',
												boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
											}}>

											<FormControl >
												<Typography variant="caption" style={{color:'#aaaaaa', textAlign: 'center'}}>
													Choose revision document (*pdf/jpg/jpeg/png)
												</Typography>
												<Row>
													<Col xs="1" sm="2">
													</Col>
													<Col xs="8" sm="7">
														<input style={{marginLeft:'0px'}}
															// accept="image/*"
															id="contained-button-file"
															placeholder="ID Card"
															name="ktp_pdf"
															type="file"
															required={true}
															onChange={this.handleFileInput.bind(this)}
														/>
													</Col>
													<Col xs="3" sm="1">
													</Col>
												</Row>
											</FormControl>

											<Button
												color="info"
												href="#pablo"
												className="btn-round"
												onClick={e => e.preventDefault()}
												size="sm"
												style={{marginBottom:'20px', backgroundColor:'#F1A124', borderColor:'#F1A124'}}
												onClick={this.handleClickUploadFile.bind(this)}
											>
												Upload Revisi
											</Button>

											<Row>
												<Col lg="12" style={{color:'#999999'}}>
													{this.state.progressStatus}
												</Col>
												{(function() {
														if(file_uploaded.id != null) {
															return(
																<Col lg="12" style={{marginBottom:'10px'}}>
																	<p style={{margin:'0px', color:'#999999', fontSize:'10px'}}>
																		File uploaded : {file_uploaded.filename}
																	</p>
																	<p style={{margin:'0px', color:'#999999', fontSize:'10px'}}>
																		Success uploaded file to server
																	</p>
																</Col>
															)
														}
												})()}
											</Row>


												{(function() {
														if(is_file_uploaded) {
															return(
																<Form className="homeGrid">
																	<FormGroup>
                                    <label htmlFor="exampleInputJudul1" style={{textAlign: 'left'}}>
            												Judul
            												</label>
																		<Input
																			id="exampleInputJudul1"
																			placeholder="Judul Project.."
																			defaultValue={idea_title_rev}
																			type="name"
																			style={{ fontFamily: 'Montserrat',
																			fontWeight:'400',
																			fontSize: '12px', color:'#2B678C'}}
																			onChange={handleChangeJudulProject}
																		></Input>
																	</FormGroup>
																	<FormGroup>
                                    <label htmlFor="exampleInputPassword1" style={{textAlign: 'left'}}>
                                      Deskripsi
                                    </label>
                                    <Input
                                      id="exampleInputPassword1"
                                      placeholder="Deskripsi Project.."
																			defaultValue={idea_desc_rev}
                                      type="textarea"
                                      style={{ height: 100, fontFamily: 'Montserrat',
																			fontWeight:'400',
																			fontSize: '12px', color:'#2B678C' }}
                                      onChange={handleChangeDeskripsiProject}>
                                    </Input>
																	</FormGroup>

																	<div style={{alignItems:'center', textAlign:'center', position:'center'}}>
																		<Button
					                            className="btn-round"
					                            color="default"
					                            type="button"
					                            style={{backgroundColor:'#F1A124', position:'center', borderColor:'#F1A124', width:'50%', marginTop:'10px'}}
																			onClick={handleSubmit}
																		>
					                            Revisi
					                          </Button>
																	</div>
																</Form>
															)
														}
												})()}

											</Card>
										</Col>
									</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
