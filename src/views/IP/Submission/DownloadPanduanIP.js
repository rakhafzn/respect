/*eslint-disable*/
import React from "react";
import { Document, Page } from 'react-pdf';

// reactstrap components
import axios from 'axios'
import Cookies from 'universal-cookie'
import { Button, Card, CardBody, CardTitle,
	Form, FormGroup, FormText, Label, Input, Container, Row, Col } from "reactstrap";
import { Link, BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
// core comments

// <div className="content-center">

export class PresentationHeaderIdeation extends React.Component {
	constructor() {
		super();
		this.state = {
			userloggedin: {},
			login_status: null,
			color: [],
		};
	}

  componentWillMount() {
		const cookies = new Cookies()
		this.setState({
			userloggedin: cookies.get('userloggedin'),
			login_status: cookies.get('login_status'),
			name: null,
			directorate: null,
			judulProject: null,
		})
	}

	componentDidMount(){
		// axios.get('https://training-api.pti-cosmetics.com/idea_submited_ip?id=eq.2')
		// .then(response => {
		// 	console.log(response.data)
		//
		// })
		// .catch(error => {
		// 	console.log(error)
		// 	console.log('GAGAL')
		// })
	}

	render(){
		// #2B678C
		// <div className="type">1.0</div>
		console.log(this.state.userloggedin)
		console.log(this.state.login_status)
		console.log(this.state.name)
		console.log(this.state.directorate)
		console.log(this.state.judulProject)

		//#2B678C
		if (this.state.login_status === undefined || this.state.login_status === null) {
      return(
        <Redirect from="*" to="/login" />
      )
    } else {
			return (
	      <>
	        <div >
	          <div
						style={{ backgroundColor: 'rgba(0,0,0,0)', padding:'0px', marginBottom:'100px'}}
	            className="section"
	          >
	            <div style={{marginTop:'10px'}}>
	              <Container>
									<h2 className="presentation-subtitle text-center" style={{marginTop:'10px', fontWeight:'600', fontSize:'16px', color:'#2B678C'}}>
	                	Download proposal A3 berikut untuk untuk submit projectmu
	                </h2>

									<div style={{alignItems:'center', justifyContent:'center', flexDirection: 'column', display: 'flex', textAlign:'center'}}>
									<Row>
	                  <Col className="ml-auto mr-auto">
											<a href="https://drive.google.com/file/d/1wUOIUGiBQA7iXmO1u6eTt5YIuXx7D_mO/view?usp=sharing" target="_blank" download>
												<Button
													style={{
														backgroundColor:"#F1A124",
														marginBottom:'10px',
														borderRadius:'20px',
														borderColor: '#F1A124',
														marginTop:'10px'
													}}
												>
													Download
												</Button>
											</a>
										</Col>
									</Row>
									</div>
								</Container>
	            </div>
	          </div>
	        </div>

	      </>
	    );
	  }
	}
}

export default PresentationHeaderIdeation;
