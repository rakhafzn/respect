import React from "react";

import ColorNavbarIP from "components/Navbars/ColorNavbarIP.js";
import SubmitForm from "views/Respect/Submit/SubmitForm.js"
import ColorNavbarRespect from "components/Navbars/ColorNavbarRespect.js";
import HeaderIP from "./IP/Submission/HeaderIP.js";
import StatusProposalIP from "./IP/Submission/StatusProposalIP.js";
import DownloadPanduanIP from "./IP/Submission/DownloadPanduanIP.js";
import FooterBlackIdeation from "components/Footers/FooterBlackIdeation.js";
import LogCR from "views/Respect/Review/LogCR.js"

function RespectSubmit() {
  document.documentElement.classList.remove("nav-open");
  // function that is being called on scroll of the page
  const checkScroll = () => {
    // it takes all the elements that have the .add-animation class on them
    const componentPosition = document.getElementsByClassName("add-animation");
    const scrollPosition = window.pageYOffset;
    for (var i = 0; i < componentPosition.length; i++) {
      var rec =
        componentPosition[i].getBoundingClientRect().top + window.scrollY + 100;
      // when the element with the .add-animation is in the scroll view,
      // the .animated class gets added to it, so it creates a nice fade in animation
      if (scrollPosition + window.innerHeight >= rec) {
        componentPosition[i].classList.add("animated");
        // when the element with the .add-animation is not in the scroll view,
        // the .animated class gets removed from it, so it creates a nice fade out animation
      } else if (scrollPosition + window.innerHeight * 0.8 < rec) {
        componentPosition[i].classList.remove("animated");
      }
    }
  };

  React.useEffect(() => {
    document.body.classList.add("presentation-page");
    window.addEventListener("scroll", checkScroll);
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("presentation-page");
      window.removeEventListener("scroll", checkScroll);
    };
  });
  return (
    <>
      <div>
        <img style={{width:'100%', zIndex:'0', position:'fixed', top:'0px'}}
          alt="..."
          src={require("assets/img/ideabox/ip_bg5.png")}
        />
        <ColorNavbarRespect />
        <LogCR />
        <FooterBlackIdeation />
      </div>
    </>
  );
}

export default RespectSubmit;
